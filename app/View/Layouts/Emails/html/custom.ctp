<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table bgcolor="#eeeeee" width="600" style="margin: 0 auto; padding: 5px; border-spacing: 0px; font-family: calibri">

			<thead bgcolor="#fff" style="background: #fff;">
				<tr>
					<th style="padding: 10px; text-align: left;">
						<center>
							<img src="http://santacasasaudesjc.com.br/images/marca-santacasasaude.png" width="250"/>
						</center>
					</th>
				</tr>
			</thead>
			<tbody bgcolor="#fff" style="background: #fff;">
				<tr>
					<td style="color:#333; padding: 10px">
						<center>
							<?php echo utf8_decode($this->fetch('content')); ?>
						</center>
					</td>
				</tr>
				<tr>
					<td bgcolor="#eeeeee" style="padding-top: 10px;">
						<center>
								Copyright &copy; <?php echo date('Y'); ?> <a href="http://santacasasaudesjc.com.br/" target="_blank">Santa Casa Sa&uacute;de</a>. 
								All rights reserved.
						</center>
					</td>
				</tr>
			</tbody>
		</table>		
	</body>
</html>