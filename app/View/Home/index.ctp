
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Santa Casa Saúde | São José dos Campos</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  	<!-- fb meta -->
  	<meta property="og:title" content="Santa Casa Saúde | São José dos Campos" />
  	<meta property="og:type" content="website" />
  	<meta property="og:url" content="http://santacasasaudesjc.com.br/" />
  	<meta property="og:image" content="" />
  	<meta property="og:description" content="Planos de Saúde: Individual, Familiar e Empresarial"/>

	<meta name="description" content="Planos de Saúde: Individual, Familiar e Empresarial"/>
	<meta name="author" content="Ok ideias">
	<link rel="canonical" href="http://santacasasaudesjc.com.br/" />
    <link href="css/flexslider.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/line-icons.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/elegant-icons.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700%7CRaleway:700" rel="stylesheet" type="text/css">
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <link rel=icon href=manager/img/favicon.png sizes="16x16" type="image/png">
</head>

<body>
    <div class="nav-container">
        <nav class="top-bar">
            <div class="container">

                <div class="row nav-menu">
                    <div class="col-md-3 columns">
                        <a href="<?php echo $url_site; ?>index.php">
                        	<img class="logo logo-light" alt="Logo" src="img/marca-santacasasaude.png">
                        	<img class="logo logo-dark" alt="Logo" src="img/marca-santacasasaude.png">
                       	</a>
                    </div>
                    <!--COLOQUE AQUI O MODAL DO APP AGENDA SAUDE
                    <div class="pull-right utility-menu">
                      <a href="#" class="btn btn-primary login-button btn-xs">Corretor</a>
                    </div>-->

                    <div class="col-md-9 columns align-vertical no-align-mobile">
                        <ul class="menu">
                            <!-- <li><a href="index.php">Início</a></li> -->
                            <!-- <li><a href="index.php">Início</a></li> -->
							<strong>
                            <li class="has-dropdown"><a>Institucional</a>
                                <ul class="subnav">
                                    <li><a href="<?php echo $url_site; ?>institucional.php">Missão, visão e valores</a></li>
                                    <li><a href="<?php echo $url_site; ?>hospital-santa-casa.php">Hospital Geral</a></li>
                                    <li><a href="<?php echo $url_site; ?>rede-medica.php">Rede médica</a></li>
                                    <li><a href="<?php echo $url_site; ?>maternidade.php">Maternidade</a></li>
                                    <li><a href="<?php echo $url_site; ?>pronto-atendimento.php">Pronto Atendimento</a></li>
                                    <li><a href="<?php echo $url_site; ?>clinicas-de-especialidades.php">Clínicas de Especialidades</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo $url_site; ?>sab.php">Atendimento ao Beneficiário</a></li>
                            <li><a href="<?php echo $url_site; ?>urgencia-e-emergencia.php">Urgência</a></li>
                            <!-- <li><a href="index.php#portalbeneficiario" class="inner-link">Serviços</a></li> -->
                            <li><a href="<?php echo $url_site; ?>comunicados.php">Comunicados</a></li>
                            <li class="has-dropdown"><a>Planos de saúde</a>
                                <ul class="subnav">
                                    <li><a href="<?php echo $url_site; ?>plano-saude-familiar.php">Plano Individual e Familiar.</a></li>
                                    <li><a href="<?php echo $url_site; ?>plano-saude-empresarial.php">Plano Empresarial</a></li>
                                    <li><a href="<?php echo $url_site; ?>plano-saude-pme.php">Plano PME</a></li>
                                    <li><a href="<?php echo $url_site; ?>minutas.php">Minutas contratuais</a></li>
                                </ul>
                            </li> 
                            <li class="has-dropdown"><a>Portal</a>
                                <ul class="subnav">
                                    <li><a href="http://200.220.137.230:65003/portal/">Portal do beneficiário</a></li>
                                    <li><a href="http://200.220.137.230:65003/bennet/">Portal corretora</a></li>
                                    <li><a href="http://200.220.137.230:65003/bennet/">Portal Empresa</a></li>
                                    <li class="has-sub-dropdown"><a>Portal Prestador</a>
                                        <ul class="subnav-2">
                                            <li><a href="http://200.220.137.230:65003/autorizanet/">Autorizações</a></li>
                                            <li><a href="http://200.220.137.230:65003/portal/">Demonstrativo</a></li>
                                            <li><a href="<?php echo $url_site; ?>tiss.php">Portal TISS</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
							<li><a href="<?php echo $url_site; ?>preventiva.php">Saber Viver</a></li>
							</strong>
                            <!--  <li><a href="dicas-saude.php">Dicas de saude</a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="mobile-toggle">
                    <i class="icon icon_menu"></i>
                </div>

            </div>
        </nav>
    </div>
    <div class="main-container">
        <header class="page-header-autorizador">
            <div class="background-image-holder" style="background: url(&quot;img/theme-legislacao.jpg&quot;) 50% 0%;">
                <img class="background-image" alt="Background Image" src="img/theme-legislacao.jpg" style="display: none;">
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-9">
                        <h1 class="text-white">AUTORIZADOR DE GUIAS</h1>

                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </header>        

        <section id="orcamento" class="">
            <div class="container">
                <?php 
                	echo $this->Form->create( 'Request', array( 'type' => 'file' ) ); 
                		$onlyView = "";
                ?>
                        <div class="contact-form default-webform">
                            <p>
                                Preencha todos os campos e clique em Enviar solicitação. Você receberá um e-mail contendo uma <strong>cópia de sua solicitação</strong>.
                            </p>
                            <p>
                                O Departamento responsável atenderá a solicitação no e-mail informado em até <strong>2 dias úteis</strong>.
                            </p>
        
                            <hr>
            
                            <div class="row">
                                <div class="col-md-6 col-xs-10"> 
                                    <?php echo $this->Form->input('Customer.card_number', array('class' => 'form-control input-lg-3', 'label' => 'Número do cartão*', 'div' => false, 'placeholder' => 'Número do cartão (digite apenas números)*', $onlyView ));?>
                                </div>
                                <div class="col-md-6 col-xs-2 tooltip-form"> 
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="O número do cartão deverá estar no seguinte formato: 0 020 ************ *">
                                        <span class="glyphicon glyphicon-question-sign"></span>
                                    </a> 
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-5"> 
                                    <!-- <input class="form-control input-lg-3" placeholder="Nome completo*" type="text"/> -->
                                    <?php echo $this->Form->input('Customer.name', array('class' => 'form-control input-lg-3', 'label' => 'Nome completo*', 'div' => false, 'placeholder' => 'Nome completo', $onlyView ));?>
                                </div>
                                <div class="col-md-4"> 
                                	<?php echo $this->Form->input('Customer.cpf', array('class' => 'form-control input-lg-3', 'label' => 'CPF*', 'div' => false, 'placeholder' => 'Seu CPF*', $onlyView ));?>
                                </div>
                                <div class="col-md-3">
                                	<?php 
                                		$op = array( '1' => 'Masculino', '2' => 'Feminino' );
                                		echo $this->Form->input('Customer.gender', array( 'options' => $op, 'class' => 'form-control form-select', 'label' => 'Sexo*', 'div' => false, 'empty' => 'Selecione seu sexo*', $onlyView ));
                                	?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
									<?php echo $this->Form->input('Customer.email', array('class' => 'form-control input-lg-3', 'label' => 'E-mail*', 'placeholder' => 'E-mail para retorno*', 'div' => false, $onlyView ));?>
                                </div>
                                <div class="col-md-4">
                                    <?php echo $this->Form->input('Customer.phone', array('class' => 'form-control input-lg-3', 'placeholder' => 'Telefone (informe DDD)*', 'label' => 'Telefone*', 'div' => false, $onlyView ));?>
                                </div>
                                <div class="col-md-4">
                                	<?php echo $this->Form->input('Customer.mobile', array('class' => 'form-control input-lg-3', 'label' => 'Celular', 'div' => false, 'placeholder' => 'Celular (informe DDD)', $onlyView ));?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $this->Form->input('local', array('class' => 'form-control input-lg-3', 'label' => 'Local do procedimento*', 'div' => false, 'placeholder' => 'Onde será realizado o procedimento?*', $onlyView ));?>
                                </div>
                                <div class="col-md-6">
                                    <label>Data do exame</label>
                                    <?php echo $this->Form->input('appointment', array('class' => 'form-control input-lg-3', 'label' => 'Data do exame', 'div' => false, $onlyView, 'type' => 'text', 'placeholder' => 'Data do exame (opcional)'));?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php echo $this->Form->input('card_photo_front', array('class' => 'form-control input-lg-3', 'label' => 'Foto do cartão', 'div' => false, $onlyView, 'type' => 'file'));?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo $this->Form->input('card_photo_back', array('class' => 'form-control input-lg-3', 'label' => 'Foto do cartão verso', 'div' => false, $onlyView, 'type' => 'file'));?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php echo $this->Form->input('guide', array('class' => 'form-control input-lg-3', 'label' => 'Guia do médico *', 'div' => false, $onlyView, 'type' => 'file'));?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 submit-exame">
                                    <input type="submit" class="btn btn-primary" value="Enviar">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <section id="portalbeneficiario" class="image-divider overlay duplicatable-content">
            <div class="background-image-holder parallax-background">
                <!-- <img class="background-image" alt="Background Image" src="img/hero13.jpg"> -->
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text-center espaco-mobile">
                        <span class="text-white alt-font">Serviços</span>
                        <h1 class="text-white title-big">Acesso fácil</h1>
                        <p class="lead text-white">O Plano Santa Casa Saúde disponibiliza a você uma série de ferramentas on-line para economizar o seu tempo.</p>
                    </div>

                </div>
                <!--end of row-->

                <div class="row">
                  <div class="col-sm-2 text-center col-sm-offset-1">
                      <div class="">
                          <a href="<?php echo $url_site; ?>sab.php"><img src="img/sab.jpg">
                              <h5 class="text-white">Serviço de atendimento ao beneficiário</h5></a>
                      </div>
                  </div>

                  <div class="col-sm-2 text-center">
                      <div class="">
                          <a href="<?php echo $url_site; ?>urgencia-e-emergencia.php"><img src="img/emergencia.jpg">
                              <h5 class="text-white">Urgência e emergência</h5></a>
                      </div>
                  </div>

                  <div class="col-sm-2 text-center">
                      <div class="">
                          <a href="<?php echo $url_site; ?>fale-conosco.php" target="_blank"><img src="img/consultas.jpg">
                              <h5 class="text-white">Fale Conosco</h5></a>
                      </div>
                  </div>

                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="http://200.220.137.230:65004/boletosmvc" target="_blank"><img src="img/boleto.jpg">
                                <h5 class="text-white">2a via de boleto</h5></a>

                        </div>
                    </div>

                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="<?php echo $url_site; ?>guia-medico.php" target="_blank"><img src="img/guia-medico.jpg">
                                <h5 class="text-white">Guia médico completo</h5></a>
                        </div>
                    </div>
                </div>
                <!--end of row-->
                <div class="row">

                    <div class="col-sm-2 text-center col-sm-offset-1">
                        <div class="">
                            <a href="<?php echo $url_site; ?>/portal.html"><img src="img/portal-beneficiario.jpg">
                                <h5 class="text-white">Portal do Beneficiário</h5></a>
                        </div>
                    </div>


                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="<?php echo $url_site; ?>/bennet.html"><img src="img/empresa.jpg">
                                <h5 class="text-white">Portal Empresa</h5></a>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="<?php echo $url_site; ?>atualizacao_cadastral.php" target="_blank"><img src="img/atualizar.jpg">
                                <h5 class="text-white">Atualize seu cadastro</h5></a>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="<?php echo $url_site; ?>parto.php" target="_blank"><img src="img/parto-adequado.jpg">
                                <h5 class="text-white">Parto Adequado</h5></a>
                        </div>
                    </div>

                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="<?php echo $url_site; ?>manual-do-beneficiario.php"><img src="img/manual-beneficiario.jpg">
                                <h5 class="text-white">Manual do beneficiário</h5></a>
                        </div>
                    </div>
                </div>
                <!--end of row-->
				 <div class="row">

                    <div class="col-sm-2 text-center col-sm-offset-1">
                        <div class="">
                            <a href="<?php echo $url_site; ?>ouvidoria.php"><img src="img/ouvidoria.jpg">
                                <h5 class="text-white">Ouvidoria</h5></a>
                        </div>
                    </div>


                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="<?php echo $url_site; ?>comunicados.php"><img src="img/comunicado.jpg">
                                <h5 class="text-white">Comunicados</h5></a>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="<?php echo $url_site; ?>preventiva.php" target="_blank"><img src="img/preventiva.jpg">
                                <h5 class="text-white">Medicina Preventiva</h5></a>
                        </div>
                    </div>
                    <div class="col-sm-2 text-center">
                        <div class="">
                            <a href="<?php echo $url_site; ?>downloads-scs/MovimentacaoRedeCredenciada.pdf" target="_blank"><img src="img/rede.jpg">
                                <h5 class="text-white">Movimentação de Rede Credenciada</h5></a>
                        </div>
                    </div>
                </div>
                <!--end of row-->

            </div>
            <!--end of container-->
        </section>

        <section class="no-pad clearfix">
            <div class="col-md-12 col-sm-12 no-pad">
                <div class="feature-box">
                    <div class="background-image-holder overlay">
                        <img class="background-image" alt="Background Image" src="img/santa-casa-saude-slide1.jpg">
                    </div>
                    <div class="container">
                        <div class="col-md-6">

                            <h2 class="text-white title-destaques">Plano Individual e Familiar: <br>A segurança de um bom atendimento 24 horas.</h2>
                            <a href="<?php echo $url_site; ?>plano-saude-familiar.php" class="btn btn-primary btn-branco">Veja mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="primary-features">
            <div class="container">
                <div class="row">
                    <h2 class="text-white text-center">O plano que funciona quando você precisa.</h2>
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>

        <section class="no-pad clearfix">
            <div class="col-md-6 col-sm-12 no-pad">

                <div class="feature-box">
                    <div class="background-image-holder overlay">
                        <img class="background-image" alt="Background Image" src="img/plano-empresarial.jpg">
                    </div>
                    <div class="inner pull-right text-right">
                        <h2 class="text-white title-destaques">Para grandes empresas: <br>A melhor gestão de custos com qualidade médica comprovada.<br></h2>
                        <a href="<?php echo $url_site; ?>plano-saude-empresarial.php" class="btn btn-primary btn-white">Veja mais</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 no-pad">
                <div class="feature-box">
                    <div class="background-image-holder overlay">
                        <img class="background-image" alt="Background Image" src="img/plano-pme.jpg">
                    </div>
                    <div class="inner">
                        <h2 class="text-white title-destaques">Para pequenas <br>e médias empresas:<br> o plano mais econômico <br>e mais completo. </h2>
                        <a href="<?php echo $url_site; ?>plano-saude-pme.php" class="btn btn-primary btn-filled">Veja mais</a>
                    </div>
                </div>
            </div>

        </section>
        <section class="duplicatable-content">
  				<div class="container">
  					<div class="row">
  						<div class="col-sm-12 text-center">
  							<h1>Notícias</h1>
  						</div>
  					</div><!--end of row-->

  					<div class="row">
  						<div class="col-md-4 col-sm-6">
  							<div class="blog-snippet-1">
  								<a href="<?php echo $url_site; ?>contratos/oficio.pdf">
  									<img alt="Blog Thumb" src="img/santacasa-saojose.jpg">
  								</a>


  							</div>
  						</div>

  						<div class="col-md-6 col-sm-8">
  							<div class="blog-snippet-1">
  								<h2>Edital de Convocação para Assembleia Geral Extraordinária</h2>
  								<span class="sub alt-font">31 de Outubro de 2017</span><br />
  								<a href="<?php echo $url_site; ?>contratos/oficio.pdf" class="btn btn-primary btn-filled btn-xs">Veja mais</a>
  							</div>
  						</div>

  					</div><!--end of row-->


									<div class="row">
  						<div class="col-md-4 col-sm-6">
  							<div class="blog-snippet-1">
  								<a href="<?php echo $url_site; ?>convenio-santa-casa-pinda.php">
  									<img alt="Blog Thumb" src="img/santa-casa-pinda.jpg">
  								</a>


  							</div>
  						</div>

  						<div class="col-md-6 col-sm-8">
  							<div class="blog-snippet-1">
  								<h2>Plano Santa Casa Saúde realiza convênio com a Santa Casa de Pindamonhangaba</h2>
  								<span class="sub alt-font">21 de agosto de 2017</span><br />
  								<a href="<?php echo $url_site; ?>convenio-santa-casa-pinda.php" class="btn btn-primary btn-filled btn-xs">Veja mais</a>
  							</div>
  						</div>

  					</div><!--end of row-->

  				</div><!--end of container-->
  			</section>
    </div>

<script>
// Validate the Entered input aganist the generated security code function
function check(input) {
  var cap =removeSpaces(document.getElementById('txtCaptcha').value);
if (input.value != cap ) {
  input.setCustomValidity("Código incorreto!");
  } else {
  // input is fine -- reset the error message
  input.setCustomValidity('');
  }
  }
 </script>


<div class="footer-container">
    <footer class="details">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                <p class="text-center"><span class="sub">© Copright 2016 Santa Casa Saúde - Todos direitos reservados CNPJ: 18.321.477/0001-34 <a href="http://santacasasaudesjc.com.br/rede-medica.php"> | Endereços e telefones </a></span> </p>
                    <ul class="social-icons">


                        <li><img src="img/ans-santa-casa-saude.png" alt=""></li>
                        <li><a href="http://www.ans.gov.br"><img src="img/ans-logo.png" alt=""></a></li>

                    </ul>
                </div>
            </div>

        </div>
        <!--end of container-->
    </footer>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/jquery.plugin.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/smooth-scroll.min.js"></script>
<script src="js/skrollr.min.js"></script>
<script src="js/spectragram.min.js"></script>
<script src="js/scrollReveal.min.js"></script>
<script src="js/isotope.min.js"></script>
<script src="js/twitterFetcher_v10_min.js"></script>
<script src="js/lightbox.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>

<?php echo $this->Html->script('mask-input/js/jquery.maskedinput'); ?>

<script src="js/scripts.js"></script>

<script type="text/javascript">
    $(function () {
    	$('[data-toggle="tooltip"]').tooltip();

    	$("#CustomerCardNumber").mask("9 999 999999999999 9");
    	$("#CustomerCpf").mask("999.999.999-99");
    	$("#CustomerPhone").mask("(99) 9999-9999");
    	$("#CustomerMobile").mask("(99) 9999-99999");
    	$("#RequestAppointment").mask("99/99/9999");

    })
</script>

</body>

</html>
