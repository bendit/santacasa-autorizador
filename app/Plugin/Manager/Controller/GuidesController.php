<?php

class GuidesController extends ManagerAppController {

	public $components = array( 'Manager.Qimage' );

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow( 'view' );
	}

	public function index(){
		$guides = $this->Guide->find( 'all' );
		$this->set( 'guides', $guides );
	}

	public function add(){
		if ( $this->request->is( 'post' ) || $this->request->is( 'PUT' ) ) {
			$this->upload();
			$this->Guide->create();
			if ( $this->Guide->save( $this->request->data ) ) {
				$this->Session->setFlash( __('Guia médico cadastrado com sucesso!'), 'default', array('class' => 'callout callout-success' ) );
				return $this->redirect( array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash( __( 'Ocorreu um erro ao cadastrar o guia médico. Por favor, tente novamente.' ), 'default', array( 'class' => 'callout callout-danger' ) );
			}
		}

		$this->render( 'form' );
	}

	public function edit( $id = null ) {
		if ( $this->request->is( 'post' ) || $this->request->is( 'PUT' ) ) {
			$this->upload();
			$this->Guide->create();
			if ( $this->Guide->save( $this->request->data ) ) {
				$this->Session->setFlash( __('Guia médico atualizado com sucesso!'), 'default', array('class' => 'callout callout-success' ) );
				return $this->redirect( array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao atualizar o guia médico. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		} else {
			$options = array( 'conditions' => array( 'Guide.' . $this->Guide->primaryKey => $id ) );
			$this->request->data = $this->Guide->find( 'first', $options );
		}

		$this->render( 'form' );
	}

    public function upload() {
    	if ( !file_exists( MANAGER_GUIDES_DIR ) ) 
    		mkdir( MANAGER_GUIDES_DIR );

		if( !empty( $this->data['Guide']['file']['name'] ) ) {
			
			$filename = $this->Qimage->copy( array( 'file' => $this->data['Guide']['file'], 'path' => MANAGER_GUIDES_DIR ) );
			unset( $this->request->data[ 'Guide' ][ 'file' ] );

			if ( $this->Qimage->getErrors() )
				unlink( MANAGER_GUIDES_DIR . $filename );
			else
				$this->request->data['Guide']['filename'] = $filename;
		}

		return true;
    }

    public function view() {
    	$this->autoRender = false;
    	$options = array( 'order' => array( 'Guide.id DESC' ) );
		$guide = $this->Guide->find( 'first' );

		$filename = MANAGER_GUIDES_DIR . $guide[ 'Guide' ][ 'filename' ];

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename={$filename}");
		@readfile( $filename );
    }

	public function delete( $id = null, $filename = null ){
		$this->Guide->id = $id;
		if ( !$this->Guide->exists() ) {
			throw new NotFoundException( __( 'Guia médico não encontrado e ocorreu um erro ao excluir o guia médico. Por favor, tente novamente.' ) );
		}
		$this->request->onlyAllow('post', 'delete');
		if ( $this->Guide->delete() ) {
			unlink( MANAGER_GUIDES_DIR . $filename );
			$this->Session->setFlash( __( 'Guia médico excluído com sucesso!' ), 'default', array( 'class' => 'callout callout-success' ) );
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash( __( 'Ocorreu um erro ao atualizar o guia médico. Por favor, tente novamente.' ), 'default', array('class' => 'callout callout-danger'));
		return $this->redirect(array('action' => 'index'));
	}
}
