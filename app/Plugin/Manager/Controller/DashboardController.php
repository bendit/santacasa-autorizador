<?php

class DashboardController extends ManagerAppController {

	public $goal, $period = 0;

	public function index(){
		$this->breadcrumb();
	}

	private function breadcrumb() {
		$arr = array(
			0 => array(
				'link' => '/',
				'texto' => '<i class="fa fa fa-dashboard"></i> Home'
			),
			1 => array(
				'link' => '',
				'texto' => 'Dashboard'
			),
		);

		$this->set('arr_navegacao', $arr);
		$this->set('titulo', 'Dashboard');
		$this->set('subTitulo', 'beta');

		$this->set('btn_add', false);	
		$this->set('btn_add_post', false);	
		
		$this->set('menu', 'dashboard');
		$this->set('subMenu', '');	
	}
}
