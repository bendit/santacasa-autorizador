<?php

class PostsController extends ManagerAppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
		$this->autoRender = false;
	}

	public function index(){
		if ( $this->request->is( 'post' ) || $this->request->is( 'PUT' ) ) {
			$this->Post->create();
			if ( !$this->Post->save( $this->request->data ) )
				throw new InternalErrorException();
		} else {
			throw new BadRequestException();
		}
	}
}
