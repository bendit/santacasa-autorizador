<?php

class UsersController extends ManagerAppController {

	public $components = array('Manager.Qimage');

	public function index(){
		$users = $this->User->find( 'all' );
		$this->set( 'users', $users );
	}

	public function add(){
		if ( $this->request->is( 'post' ) || $this->request->is( 'PUT' ) ) {
			$this->User->create();
			if ( $this->User->save( $this->request->data ) ) {
				$this->Session->setFlash( __('Usuário cadastrado com sucesso!'), 'default', array('class' => 'callout callout-success' ) );
				return $this->redirect( array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash( __( 'Ocorreu um erro ao cadastrar o usuário. Por favor, tente novamente.' ), 'default', array( 'class' => 'callout callout-danger' ) );
			}
		}

		$this->render( 'form' );
	}

	public function edit( $id = null ) {
		if ( $this->request->is( 'post' ) || $this->request->is( 'PUT' ) ) {
			$this->User->create();
			if ( $this->User->save( $this->request->data ) ) {
				$this->Session->setFlash( __( 'Usuário atualizado com sucesso!' ), 'default', array( 'class' => 'callout callout-success' ) );
				return $this->redirect( array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao atualizar o usuário. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		} else {
			$options = array( 'conditions' => array( 'User.' . $this->User->primaryKey => $id ) );
			$this->request->data = $this->User->find( 'first', $options );
		}

		$this->render( 'form' );
	}

	public function delete( $id = null ){
		$this->User->id = $id;
		if ( !$this->User->exists() ) {
			throw new NotFoundException( __( 'Usuário não encontrado e ocorreu um erro ao excluir o usuário. Por favor, tente novamente.' ) );
		}
		$this->request->onlyAllow('post', 'delete');
		if ( $this->User->delete() ) {
			$this->Session->setFlash( __( 'Usuário excluído com sucesso!' ), 'default', array( 'class' => 'callout callout-success' ) );
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash( __( 'Ocorreu um erro ao atualizar o usuário. Por favor, tente novamente.' ), 'default', array('class' => 'callout callout-danger'));
		return $this->redirect(array('action' => 'index'));
	}

	public function profile(){
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->upload();
			
			if ($this->User->save($this->request->data)) {
				$this->Session->write('Auth.User.name', $this->request->data['User']['name']);
				$this->Session->write('Auth.User.email', $this->request->data['User']['email']);

				if ( isset( $this->request->data['User']['photo'] ) )
					$this->Session->write('Auth.User.photo', $this->request->data['User']['photo']);

				$this->Session->setFlash(__('Perfil salvo com sucesso!'), 'default', array('class' => 'callout callout-success'));
				return $this->redirect(array('action' => 'profile'));
			} else {
				$this->Session->setFlash(__('O Perfil não pôde ser salvo. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		}
		
		$options = array('conditions' => array('User.' . $this->User->primaryKey => AuthComponent::user('id')));
		$this->request->data = $this->User->find('first', $options);
	}

    public function upload() {
    	if ( !file_exists( MANAGER_USER_PHOTOS_FILES ) ) 
    		mkdir( MANAGER_USER_PHOTOS_FILES );

		if( !empty( $this->data['User']['photo_']['name'] ) ) {
			
			$filename = $this->Qimage->copy( array( 'file' => $this->data['User']['photo_'], 'path' => MANAGER_USER_PHOTOS_FILES ) );
			unset( $this->request->data[ 'User' ][ 'photo_' ] );

			if ( $this->Qimage->getErrors() ) {
				unlink( MANAGER_USER_PHOTOS_FILES . $filename );
				return false;
			} else {
				if ( file_exists( MANAGER_USER_PHOTOS_FILES . AuthComponent::user('photo') ) )
					unlink( MANAGER_USER_PHOTOS_FILES . AuthComponent::user('photo') );
				
				$this->request->data['User']['photo'] = $filename;
				return true;
			}
		}

		return true;
    }	
}
