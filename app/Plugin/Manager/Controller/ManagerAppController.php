<?php
App::uses('Controller', 'Controller');
class ManagerAppController extends AppController {

	public $components = array(
        "Session",
        'Auth' => array(
        	"loginAction" => array(
				"controller" => "login",
				"action" => "index",
				"admin" => false
			),
            'loginRedirect' => array(
                'controller' => 'dashboard',
                'action' => 'index'
            ),
            'logoutRedirect' => array(
                'controller' => 'login',
                'action' => 'index'
            ),
            "authenticate" => array(
                "Form" => array(
                    "userModel" => "User",
                    "scope" => array("User.status" => "Active"),
                    "fields" => array(
                        "username" => "email",
                        "password" => "password"
                    )
                )
            )
        ),
        'Cookie'
    );
}
