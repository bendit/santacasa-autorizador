<?php

class RequestsController extends ManagerAppController {

	public $components = array( 'Manager.Qimage' );

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow( 'download' );
	}

	public function index(){
		$options = array( 'conditions' => array( 'Request.status !=' => 'deleted' ) );
		$requests = $this->Request->find( 'all', $options );
		$this->set( 'requests', $requests );
	}

	public function add(){
		if ( $this->request->is( 'post' ) || $this->request->is( 'PUT' ) ) {
			$this->upload();
			$this->Request->create();
			if ( $this->Request->saveAssociated( $this->request->data ) ) {
				$this->Session->setFlash( __('Solicitação cadastrada com sucesso!'), 'default', array('class' => 'callout callout-success' ) );
				return $this->redirect( array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash( __( 'Ocorreu um erro ao cadastrar a solicitação. Por favor, tente novamente.' ), 'default', array( 'class' => 'callout callout-danger' ) );
			}
		}

		$this->render( 'form' );
	}

	public function edit( $id = null ) {
		if ( $this->request->is( 'post' ) || $this->request->is( 'PUT' ) ) {
			$this->Request->create();
			if ( $this->Request->saveAssociated( $this->request->data ) ) {
				$this->Session->setFlash( __('Solicitação atualizada com sucesso!'), 'default', array('class' => 'callout callout-success' ) );
				return $this->redirect( array( 'action' => 'index' ) );
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao atualizar a solicitação. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
			}
		} else {
			$options = array( 'conditions' => array( 'Request.' . $this->Request->primaryKey => $id ) );
			$this->request->data = $this->Request->find( 'first', $options );
		}

		$this->set( 'onlyView', array() );

		$this->render( 'form' );
	}

	public function view( $id = null ) {
		$options = array( 'conditions' => array( 'Request.' . $this->Request->primaryKey => $id ) );
		$this->request->data = $this->Request->find( 'first', $options );

		$this->set( 'onlyView', array( 'disabled' => 'disabled' ) );

		$this->render( 'form' );
	}

	public function authorize( $id = null ) {
		if ( $this->request->is( 'post' ) || $this->request->is( 'PUT' ) ) {
			if( !empty( $this->data['Request']['authorized_doc']['name'] ) ) {
				if ( $this->upload( $id ) ) {
					$this->Request->create();
					if ( $this->Request->saveAssociated( $this->request->data ) ) {
						$this->Session->setFlash( __('Solicitação autorizada com sucesso!'), 'default', array('class' => 'callout callout-success' ) );
						return $this->redirect( array( 'action' => 'index' ) );
					} else {
						$this->Session->setFlash(__('Ocorreu um erro ao autorizar a solicitação. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
					}
				} else {
					$this->Session->setFlash(__('Ocorreu um erro ao fazer o upload da autorização da solicitação. Por favor, tente novamente.'), 'default', array('class' => 'callout callout-danger'));
				}
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao autorizar a solicitação. Guia autorizada não foi enviada.'), 'default', array('class' => 'callout callout-danger'));
			}
		}

		$options = array( 'conditions' => array( 'Request.' . $this->Request->primaryKey => $id ) );
		$this->request->data = $this->Request->find( 'first', $options );

		$this->set( 'onlyView', array( 'disabled' => 'disabled' ) );

		$this->render( 'form' );
	}

    public function upload( $id = null ) {
    	$dir = MANAGER_REQUESTS_DIR . $id . DS . 'authorized_doc' . DS;

    	if ( !file_exists( $dir ) ) 
    		mkdir( $dir );

		if( !empty( $this->data['Request']['authorized_doc']['name'] ) ) {
			
			$filename = $this->Qimage->copy( array( 'file' => $this->data['Request']['authorized_doc'], 'path' => $dir ) );
			unset( $this->request->data[ 'Request' ][ 'authorized_doc' ] );

			if ( $this->Qimage->getErrors() ) {
				unlink( $dir . $filename );
				return false;
			} else {
				$this->request->data['Request']['authorized_doc'] = $filename;
				return true;
			}
		}

		return true;
    }

	public function email( $id = null ){
		$this->autoRender = false;
		$this->Request->id = $id;
		if ( !$this->Request->exists() ) {
			throw new NotFoundException( __( 'Solicitação não encontrada. Por favor, tente novamente.' ) );
		}
		
		$options = array( 'conditions' => array( 'Request.' . $this->Request->primaryKey => $id ) );
		$this->request->data = $this->Request->find( 'first', $options );

		$link = FULL_BASE_URL . '/autorizacao/' . $this->data[ 'Request' ][ 'hash' ];

		$msg = 'Olá <strong>' . $this->data[ 'Customer' ][ 'name' ] . '</strong>,<br />';
		$msg .= 'Sua solicitação de autorização de exame n<sup>o</sup>: ' . $id . ' <strong>foi aprovada</strong>. <br />
				Clique no link abaixo para efetuar o download da autorização. <br />
				<a href="' . $link . '" target="_blank">Guia de Autorização de Exame</a> <br /><br />
				<strong>*</strong> Caso o link não funcione, copie o endereço abaixo e cole em seu navegador. <br />
				' . $link . ' <br /><br />
				';

		$Email = new CakeEmail();
		$Email->config('smtp');
		$Email->to( $this->data[ 'Customer' ][ 'email' ] );
		$Email->subject('Santa Casa Saúde - Autorização de Exame');

		if ( $Email->send( $msg ) ) {
			$this->Session->setFlash( __( 'E-mail enviado com sucesso!' ), 'default', array( 'class' => 'callout callout-success' ) );
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash( __( 'Ocorreu um erro ao enviar e-mail. Por favor, tente novamente.' ), 'default', array('class' => 'callout callout-danger'));
		return $this->redirect(array('action' => 'index'));
	}

	public function download( $hash = null ){
		$this->autoRender = false;

		if ( !$hash )
			throw new NotFoundException( __( 'Código incorreto. Por favor, tente novamente.' ) );	

		$options = array( 'conditions' => array( 'Request.hash' => $hash ) );
		$request = $this->Request->find( 'first', $options );

		if ( $request ) {
			$filepath = MANAGER_REQUESTS_DIR . $request[ 'Request' ][ 'id' ] . DS . 'authorized_doc' . DS . $request[ 'Request' ][ 'authorized_doc' ];
			$extension= substr( $filepath, -4, 4 );

			$filename = 'Guia_Autorizada_Solicitacao_' . $request[ 'Request' ][ 'id' ] . $extension;

			if ( file_exists( $filepath ) ) {
				header('Content-Description: File Transfer');
				header('Content-Disposition: attachment; filename="' . $filename . '"');
				header('Content-Type: application/octet-stream');
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: ' . filesize( $filepath ) );
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				header('Expires: 0');

				readfile( $filepath );
			} else {
				throw new NotFoundException( __( 'Arquivo não encontrado. Por favor, tente novamente.' ) );
			}
		} else {
			throw new NotFoundException( __( 'Solicitação não encontrada. Por favor, tente novamente.' ) );	
		}
	}


	public function delete( $id = null, $filename = null ){
		$this->Request->id = $id;
		if ( !$this->Request->exists() ) {
			throw new NotFoundException( __( 'Solicitação não encontrada e ocorreu um erro ao excluir a solicitação. Por favor, tente novamente.' ) );
		}
		
		if (  $this->Request->saveField('status', 'deleted' ) ) {
			$this->Session->setFlash( __( 'Solicitação excluída com sucesso!' ), 'default', array( 'class' => 'callout callout-success' ) );
			return $this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash( __( 'Ocorreu um erro ao atualizar a solicitação. Por favor, tente novamente.' ), 'default', array('class' => 'callout callout-danger'));
		return $this->redirect(array('action' => 'index'));
	}

	public function extract(){
		$this->autoRender = false;
		$this->loadModel( 'Post' );
		$posts = $this->Post->find( 'all' );
		
		foreach (	$posts as $p ) {
			$p[ 'Post' ][ 'post' ] = json_decode( $p[ 'Post' ][ 'post' ], true );

			$d = array();
			// Dados do Cliente
			$this->request->data[ 'Customer' ][ 'card_number' ] = $p[ 'Post' ][ 'post' ][ 'numeroCartao' ];
			$this->request->data[ 'Customer' ][ 'name' ] 		= $p[ 'Post' ][ 'post' ][ 'nome' ];
			$this->request->data[ 'Customer' ][ 'cpf' ] 		= $p[ 'Post' ][ 'post' ][ 'cpf' ];
			$this->request->data[ 'Customer' ][ 'gender' ] 	  	= $p[ 'Post' ][ 'post' ][ 'sexo' ];
			$this->request->data[ 'Customer' ][ 'email' ] 	  	= $p[ 'Post' ][ 'post' ][ 'email' ];
			$this->request->data[ 'Customer' ][ 'phone' ] 	  	= $p[ 'Post' ][ 'post' ][ 'telefone' ];
			$this->request->data[ 'Customer' ][ 'mobile' ] 	  	= $p[ 'Post' ][ 'post' ][ 'celular' ];

			// Dados da Solicitação
			$this->request->data[ 'Request' ][ 'local' ] 		= $p[ 'Post' ][ 'post' ][ 'localizacao' ];
			$this->request->data[ 'Request' ][ 'appointment' ] 	= $p[ 'Post' ][ 'post' ][ 'data' ];
			$this->request->data[ 'Request' ][ 'card_photo_front_full' ] 	  	= $p[ 'Post' ][ 'post' ][ 'cartao' ];
			$this->request->data[ 'Request' ][ 'card_photo_back_full' ] 	  	= $p[ 'Post' ][ 'post' ][ 'cartao_Verso' ];
			$this->request->data[ 'Request' ][ 'guide_full' ] 	  	= $p[ 'Post' ][ 'post' ][ 'guia_medico' ];
			$this->request->data[ 'Request' ][ 'status' ] 	  	= 'new';
			$this->request->data[ 'Request' ][ 'device' ] 	  	= 'app';

			// Verifica se já existe um customer
			$this->loadModel( 'Customer' );
			$customer = $this->Customer->findByCpf( $this->data[ 'Customer' ][ 'cpf' ] );
			$this->request->data[ 'Customer' ][ 'id' ] 		  = ( $customer ) ? $customer[ 'Customer' ][ 'id' ] : null;
			$this->request->data[ 'Request' ][ 'customer_id' ] = ( $customer ) ? $customer[ 'Customer' ][ 'id' ] : null;

			if ( $this->Request->saveAssociated( $this->request->data ) ) {
				$this->Post->delete( $p[ 'Post' ][ 'id' ] );
			}
		}
	}
}
