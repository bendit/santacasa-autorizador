<?php

class LoginController extends ManagerAppController {

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow( 'recuperar' );
	}

	public function index(){
		$this->layout = false;

	    if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
	            return $this->redirect('/manager/');
	        }
	       	$this->Session->setFlash(__('Os dados de acesso estão incorretos.'), 'default', array('class' => 'callout callout-danger'));
	    }
	}

	public function logout() {
	    return $this->redirect($this->Auth->logout());
	}

/**
 * recuperar method
 *
 * @return void
 */
	public function recuperar() {
		$this->layout = false;
		$this->loadModel('User');

		if ($this->request->is('post')) {			
			$this->data = $this->User->findByEmail($this->data['User']['email']);

			if (isset($this->data['User'])) {
				$novaSenha = $this->novaSenha();
				$this->request->data['User']['password'] = $novaSenha;
				if ($this->User->save($this->request->data)) {

					$msg = 'Nova Senha: ' . $novaSenha;
					$Email = new CakeEmail();
					$Email->config('smtp');
					$Email->to($this->request->data['User']['email']);
					$Email->subject('Nova Senha');
					$Email->send($msg);
	       			$this->Session->setFlash('Foi enviado para o seu e-mail uma nova senha.', 'default', array('class' => 'callout callout-success'));
					return $this->redirect(array('controller' => 'Login', 'action' => 'index'));
				}
			} else {
      			$this->Session->setFlash('Desculpe. Mas não encontramos o e-mail preenchido.', 'default', array('class' => 'callout callout-danger'));
				return $this->redirect(array('controller' => 'Login', 'action' => 'index'));
			}
		}
	}

/**
 * nova senha method
 *
 * @return string
 */
	private function novaSenha() {
		return substr( str_shuffle( 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$' ) , 0 , 10 ); 
	}
}
