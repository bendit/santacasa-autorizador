<?php echo $this->element('navegacao'); ?>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $dashboardTitle;?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="modal" data-target="#myModal"><i class="fa fa-wrench"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
      </div>
    </div>
  </div>
  <?php echo $this->element( 'Dashboard/traffics' );?>
  <?php echo $this->element( 'Dashboard/metrics' );?>
  <?php echo $this->element( 'Dashboard/top_metrics' );?>
  <?php echo $this->element( 'Dashboard/sec_metrics' );?>
</section>

<!-- Modal -->
<?php echo $this->Form->create($model = false, array( 'type' => 'get', 'id' => 'search', 'url' => array( 'controller' => 'dashboard' ) ));?>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Filtros Avançados</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-4">
              <div class="form-group">
                <?php 
                  $label = "Tipo de Conversão";
                  echo $this->Form->input('goal', array('class' => 'form-control select2', 'label' => $label, 'div' => false, 'empty' => 'Selecione...', 'escape' => false, 'options' => $goals, 'default' => $_GET[ 'goal' ] ));
                ?>
              </div><!-- /.form-group -->
            </div>

            <div class="col-md-3">
              <div class="input-group">
                <label>Período</label>
                <button type="button" class="btn btn-default pull-right btn-flat" id="daterange-btn">
                  <i class="fa fa-calendar"></i>
                  <span>
                    <?php 

                      if ( !isset( $_GET[ 'period' ] ) || $_GET[ 'period' ] == 0 )
                        echo 'Hoje';
                      elseif ( $_GET[ 'period' ] == 1 )
                        echo 'Ontem';
                      else
                        echo 'Últimos ' . $_GET[ 'period' ] . ' Dias';

                    ?>
                  </span>
                  <i class="fa fa-caret-down"></i>
                </button>
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="goal_name" id="goal_name" value="<?php echo ( isset( $_GET[ 'goal_name' ] ) ) ? $_GET[ 'goal_name' ] : '' ;?>">
        <input type="hidden" name="period" id="period" value="<?php echo ( isset( $_GET[ 'period' ] ) ) ? $_GET[ 'period' ] : 1 ;?>">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Apply</button>
      </div>
    </div>
  </div>
</div>
<?php echo $this->Form->end();?>

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->css(array('plugins/daterangepicker/daterangepicker-bs3'));?>
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>
<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>

<!-- Sparkline -->
<?php echo $this->Html->script('plugins/sparkline/jquery.sparkline.min.js');?>
<!-- jvectormap -->
<?php echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>
<?php echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>
<!-- SlimScroll 1.3.0 -->
<?php echo $this->Html->script('plugins/slimScroll/jquery.slimscroll.min.js');?>
<!-- ChartJS 1.0.1 -->
<?php echo $this->Html->script('plugins/chartjs/Chart.min.js');?>

<!-- AdminLTE for demo purposes -->
<?php echo $this->Html->script('dist/js/demo'); ?>

<!-- date-range-picker -->
<?php echo $this->Html->script('plugins/daterangepicker/moment'); ?>
<?php echo $this->Html->script('plugins/daterangepicker/daterangepicker'); ?>

<script type="text/javascript">
  $(function () {

    $('.start_date').daterangepicker({
      "singleDatePicker": true,     
      "autoUpdateInput": false,
      format: 'YYYY-MM-DD'
    }, function(start, end, label) {
      $('.start_date').val(start.format('YYYY-MM-DD'));
    });

    $('.end_date').daterangepicker({
      "singleDatePicker": true,     
      "autoUpdateInput": false,
      format: 'YYYY-MM-DD'
    }, function(start, end, label) {
      $('.end_date').val(start.format('YYYY-MM-DD'));
    });

    $('#daterange-btn').daterangepicker(
      {
        locale: {
        format: 'YYYY-MM-DD'
      },
        ranges: {
          'Hoje': [moment(), moment()],
          'Ontem': [moment().subtract(1, 'days'), moment()],
          'Últimos 7 Dias': [moment().subtract(7, 'days'), moment()],
          'Últimos 15 Dias': [moment().subtract(15, 'days'), moment()],
          'Últimos 30 Dias': [moment().subtract(30, 'days'), moment()]
        }
      },
      function (start, end) {
        // end - start returns difference in milliseconds 
        var diff = new Date(end - start);

        // get days
        var days = parseInt(diff/1000/60/60/24);

        console.log(days);

        if ( days == 0 ) {
          days = 0;
          $('#daterange-btn span').html('Hoje');
        } else if ( days == 1 ) {
          $('#daterange-btn span').html('Ontem');
        } else {
          $('#daterange-btn span').html('Últimos ' + days + ' Dias');
        }

        $( '#period' ).val( days );
      }
    );

  // Hide Custom Date in Filter
  $( ".ranges" ).find( "li" ).last().hide();
  $( "#goal" ).change( function(){
    $( "#goal_name" ).val( $( "#goal option:selected" ).text() );
  });

  'use strict';

  /* ChartJS
   * -------
   * Here we will create a few charts using ChartJS
   */

  //-----------------------
  //- MONTHLY SALES CHART -
  //-----------------------

  // Get context with jQuery - using jQuery's .get() method.
  var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
  // This will get the first returned node in the jQuery collection.
  var salesChart = new Chart(salesChartCanvas);

  var salesChartData = {
    labels: [<?php echo $cpaEvolutions[ 'labels' ];?>],
    datasets: [
      {
        label: "CPA",
        fillColor: "rgba(60,141,188,0.9)",
        strokeColor: "rgba(60,141,188,0.8)",
        pointColor: "#3b8bba",
        pointStrokeColor: "rgba(60,141,188,1)",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(60,141,188,1)",
        data: [<?php echo $cpaEvolutions[ 'datasets' ];?>]
      }
    ]
  };

  var salesChartOptions = {
    //Boolean - If we should show the scale at all
    showScale: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - Whether the line is curved between points
    bezierCurve: true,
    //Number - Tension of the bezier curve between points
    bezierCurveTension: 0.3,
    //Boolean - Whether to show a dot for each point
    pointDot: false,
    //Number - Radius of each point dot in pixels
    pointDotRadius: 4,
    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth: 1,
    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius: 20,
    //Boolean - Whether to show a stroke for datasets
    datasetStroke: true,
    //Number - Pixel width of dataset stroke
    datasetStrokeWidth: 2,
    //Boolean - Whether to fill the dataset with a color
    datasetFill: true,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio: true,
    //Boolean - whether to make the chart responsive to window resizing
    responsive: true
  };

  //Create the line chart
  salesChart.Line(salesChartData, salesChartOptions);

  //---------------------------
  //- END MONTHLY SALES CHART -
  //---------------------------
});  
</script>