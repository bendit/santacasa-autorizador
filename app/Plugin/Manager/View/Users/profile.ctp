<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Usuários
	</h1>
	<ol class="breadcrumb">
		<li class='active'><i class="fa fa fa-users"></i>&nbsp;Home</li>
		<li>Usuários</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<!-- Profile Image -->
			<div class="box box-primary">
				<div class="box-body box-profile">
					<?php echo $this->Html->image( MANAGER_USER_PHOTOS_FILES_URL . $this->data[ 'User' ][ 'photo' ], array('alt' => 'User Image', 'class' => 'profile-user-img img-responsive img-circle'));?>
					<h3 class="profile-username text-center"><?php echo $this->data[ 'User' ][ 'name' ]; ?></h3>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
		        		<?php echo $this->Form->create('User', array( 'type' => 'file' )); ?>
		        			<?php echo $this->Form->input('id');?>
		        			<?php echo $this->Form->input( 'password_', array( 'type' => 'hidden' ) );?>
							<div class="col-md-12">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title">Editar Perfil de Usuário</h3>
										<div class="box-tools pull-right">
										</div>
									</div><!-- /.box-header -->
									<div class="box-body">
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Nome', 'div' => false));?>
												</div>
												<!-- /.form-group -->
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => 'E-mail', 'div' => false));?>
												</div>
												<!-- /.form-group -->
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => 'Senha', 'div' => false));?>
												</div>
												<!-- /.form-group -->
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label>Foto</label>
													<?php echo $this->Form->file('photo_', array('class' => 'form-control', 'label' => false)); ?>
												</div>
												<!-- /.form-group -->
											</div><!-- /.col -->
										</div><!-- /.row -->
									</div><!-- /.box-body -->
									<div class="box-footer">
							            <?php
											echo $this->Form->button(
											  '<i class="fa fa-check"></i> Salvar',
											  array('class' => 'btn btn-success ', 'escape' => false)
											);
							            ?>
							        </div>			
								</div><!-- /.box -->
							</div>
		        		<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('/manager/js/plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('/manager/js/bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->
<script src="/manager/js/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<?php echo $this->Html->script('/manager/js/dist/js/app.min'); ?>