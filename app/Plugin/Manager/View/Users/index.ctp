<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Usuários
		<?php echo $this->Html->link( '<i class="fa fa-pencil"></i> Novo', 'add', array('class' => 'btn btn-primary btn-xs', 'escape' => false ) );?>
	</h1>
	<ol class="breadcrumb">
		<li class='active'><i class="fa fa fa-users"></i>&nbsp;Home</li>
		<li>Usuários</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header box-header-dataTable">
					<h3 class="box-title"></h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<table class="table table-hover" id="data">
						<thead>
							<tr>
								<th>Usuário</th>
								<th>E-mail</th>
								<th>Status</th>
								<th>Tipo</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ( $users as $u ) {
							?>
							<tr>
								<td><?php echo $u[ 'User' ][ 'name' ];?></td>
								<td><?php echo $u[ 'User' ][ 'email' ];?></td>
								<td><?php echo $u[ 'User' ][ 'status_description' ];?></td>
								<td><?php echo $u[ 'User' ][ 'type_description' ];?></td>
								<td>
									<?php echo $this->Html->link( '<i class="fa fa-pencil"></i> Editar', array( 'action' => 'edit', $u[ 'User' ][ 'id' ] ), array('class' => 'btn btn-warning btn-xs', 'escape' => false ) );?>
									<?php 
									echo $this->Form->postLink( '<i class="fa fa-times"></i> Excluir', array('action' => 'delete', $u['User']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Tem certeza que deseja excluir o usuário # %s?', $u['User']['name']) ); 
								?>
								</td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>
<!-- Datatables -->
<?php echo $this->Html->css(array('/manager/css/plugins/datatables/dataTables.bootstrap.css'));?>
<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('/manager/js/plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('/manager/js/bootstrap/js/bootstrap.min'); ?>
<?php echo $this->Html->script('/manager/js/plugins/datatables/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('/manager/js/plugins/datatables/dataTables.bootstrap.min.js'); ?>
<?php echo $this->Html->script('/manager/js/plugins/slimScroll/jquery.slimscroll.min.js');?>
<?php echo $this->Html->script('/manager/js/plugins/fastclick/fastclick.min.js');?>
<!-- AdminLTE App -->
<?php echo $this->Html->script('/manager/js/dist/js/app.min'); ?>


<script>
  	$( function() {
		$('#data').DataTable({
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_ resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": "Próximo",
			        "sPrevious": "Anterior",
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}   
		});
 	});
</script>
