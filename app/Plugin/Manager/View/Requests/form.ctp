<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Solicitações
	</h1>
	<ol class="breadcrumb">
		<li class='active'><i class="fa fa-line-chart"></i>&nbsp;Home</li>
		<li>Solicitações</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
        		<?php echo $this->Form->create( 'Request', array( 'type' => 'file' ) ); ?>
        			<?php echo $this->Form->input('id');?>
        			<?php echo $this->Form->input('Customer.id');?>
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
									<h3 class="box-title">
										<?php 
										if ( $this->request->action == 'authorize' )
											echo 'Autorizar Solicitação';
										elseif ( empty( $onlyView ) ) 
											echo 'Editar Solicitação'; 
										else 
											echo 'Visualizar Solicitação';
										?>
									</h3>
								<div class="box-tools pull-right">
								</div>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('Customer.card_number', array('class' => 'form-control', 'label' => 'Número do cartão', 'div' => false, $onlyView ));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('Customer.name', array('class' => 'form-control', 'label' => 'Nome completo', 'div' => false, $onlyView ));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('Customer.cpf', array('class' => 'form-control', 'label' => 'CPF', 'div' => false, $onlyView ));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('Customer.gender', array('class' => 'form-control', 'label' => 'Sexo', 'div' => false, $onlyView ));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('Customer.email', array('class' => 'form-control', 'label' => 'E-mail', 'div' => false, $onlyView ));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('Customer.phone', array('class' => 'form-control', 'label' => 'Telefone', 'div' => false, $onlyView ));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('Customer.mobile', array('class' => 'form-control', 'label' => 'Celular', 'div' => false, $onlyView ));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('local', array('class' => 'form-control', 'label' => 'Local do procedimento', 'div' => false, $onlyView ));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-4">
										<div class="form-group">
											<?php echo $this->Form->input('appointment', array('class' => 'form-control', 'label' => 'Data do exame', 'div' => false, $onlyView, 'type' => 'text'));?>
										</div>
										<!-- /.form-group -->
									</div>
								</div><!-- /.row -->
							</div><!-- /.box-body -->

							<div class="box-header">
								<h3 class="box-title">Documentos</h3>
							</div><!-- /.box-header -->

							<div class="box-body bg-gray">
								<div class="row">
									<div class="col-md-12" style="padding-top: 20px;">
										<div class="col-md-4">
										<!-- Box Comment -->
											<div class="box box-widget collapsed-box">
												<div class="box-header with-border">
													<div class="user-block">
														<img class="img-circle" src="<?php echo MANAGER_FILES_URL . 'padrao.png';?>">
														<span class="username"><a href="#"><?php echo $this->data[ 'Customer' ][ 'name' ];?></a></span>
														<span class="description">Foto do cartão - <?php echo $this->data[ 'Customer' ][ 'card_number' ];?></span>
													</div>
													<!-- /.user-block -->
													<div class="box-tools">
														<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
													</div>
													<!-- /.box-tools -->
												</div>
												<!-- /.box-header -->
												<div class="box-body">
													<?php $link = MANAGER_REQUESTS_URL . $this->data[ 'Request' ][ 'id' ] . '/'  . $this->data[ 'Request' ][ 'card_photo_front' ];?>
													<a href="<?php echo $link;?>" target="_blank">
														<img class="img-responsive pad" src="<?php echo $link;?>" alt="Foto do cartão">
													</a>
												</div>
												<!-- /.box-body -->
											</div>
										<!-- /.box -->
										</div>

										<div class="col-md-4">
										<!-- Box Comment -->
											<div class="box box-widget collapsed-box">
												<div class="box-header with-border">
													<div class="user-block">
														<img class="img-circle" src="<?php echo MANAGER_FILES_URL . 'padrao.png';?>">
														<span class="username"><a href="#"><?php echo $this->data[ 'Customer' ][ 'name' ];?></a></span>
														<span class="description">Foto do verso cartão - <?php echo $this->data[ 'Customer' ][ 'card_number' ];?></span>
													</div>
													<!-- /.user-block -->
													<div class="box-tools">
														<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
													</div>
													<!-- /.box-tools -->
												</div>
												<!-- /.box-header -->
												<div class="box-body">
													<?php $link = MANAGER_REQUESTS_URL . $this->data[ 'Request' ][ 'id' ] . '/'  . $this->data[ 'Request' ][ 'card_photo_back' ];?>
													<a href="<?php echo $link;?>" target="_blank">
														<img class="img-responsive pad" src="<?php echo $link;?>" alt="Foto do cartão">
													</a>
												</div>
												<!-- /.box-body -->
											</div>
										<!-- /.box -->
										</div>

										<div class="col-md-4">
										<!-- Box Comment -->
											<div class="box box-widget collapsed-box">
												<div class="box-header with-border">
													<div class="user-block">
														<img class="img-circle" src="<?php echo MANAGER_FILES_URL . 'padrao.png';?>">
														<span class="username"><a href="#"><?php echo $this->data[ 'Customer' ][ 'name' ];?></a></span>
														<span class="description">Foto da Guia do Médico - <?php echo $this->data[ 'Customer' ][ 'card_number' ];?></span>
													</div>
													<!-- /.user-block -->
													<div class="box-tools">
														<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
													</div>
													<!-- /.box-tools -->
												</div>
												<!-- /.box-header -->
												<div class="box-body">
													<?php $link = MANAGER_REQUESTS_URL . $this->data[ 'Request' ][ 'id' ] . '/'  . $this->data[ 'Request' ][ 'guide' ];?>
													<a href="<?php echo $link;?>" target="_blank">
														<img class="img-responsive pad" src="<?php echo $link;?>" alt="Foto do cartão">
													</a>
												</div>
												<!-- /.box-body -->
											</div>
										<!-- /.box -->
										</div>
									</div>
								</div><!-- /.row -->
							</div><!-- /.box-body -->

							<?php
							if ( $this->request->action == 'authorize' ) {
								echo $this->Form->hidden( 'user_id', array( 'value' => AuthComponent::user( 'id' ) ) );
								echo $this->Form->hidden( 'status', array( 'value' => 'closed' ) );
								echo $this->Form->hidden( 'authorized_date', array( 'value' => date( 'Y-m-d H:i:s' ) ) );
							?>

							<div class="box-header">
								<h3 class="box-title">Autorização</h3>
							</div><!-- /.box-header -->

							<div class="box-body bg-gray">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label>Guia Autorizada</label>
												<?php echo $this->Form->file('authorized_doc', array('class' => 'form-control', 'label' => 'Guia Autorizada', 'div' => false));?>
											</div>
											<!-- /.form-group -->
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('authorized_by', array('class' => 'form-control', 'label' => 'Autorizado por', 'div' => false, 'value' => AuthComponent::user( 'name' ), 'disabled' => 'disabled'));?>
											</div>
											<!-- /.form-group -->
										</div>
									</div>
								</div><!-- /.row -->
							</div><!-- /.box-body -->

							<?php
							}
							?>

							<?php
							if ( $this->request->action == 'view' && $this->data[ 'Request' ][ 'status' ] == 'closed' ) {
							?>

							<div class="box-header">
								<h3 class="box-title">Autorização</h3>
							</div><!-- /.box-header -->

							<div class="box-body bg-gray">
								<div class="row">
									<div class="col-md-12">
										<div class="col-md-6" style="margin-top: 20px;">
										<!-- Box Comment -->
											<div class="box box-widget collapsed-box">
												<div class="box-header with-border">
													<div class="user-block">
														<img class="img-circle" src="<?php echo MANAGER_FILES_URL . 'padrao.png';?>">
														<span class="username"><a href="#"><?php echo $this->data[ 'Customer' ][ 'name' ];?></a></span>
														<span class="description">Guia autorizada - <?php echo $this->data[ 'Customer' ][ 'card_number' ];?></span>
													</div>
													<!-- /.user-block -->
													<div class="box-tools">
														<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
													</div>
													<!-- /.box-tools -->
												</div>
												<!-- /.box-header -->
												<div class="box-body">
													<?php $link = MANAGER_REQUESTS_URL . $this->data[ 'Request' ][ 'id' ] . '/authorized_doc/'  . $this->data[ 'Request' ][ 'authorized_doc' ];?>
													<a href="<?php echo $link;?>" target="_blank">
														<img class="img-responsive pad" src="<?php echo $link;?>" alt="Guia autorizada">
													</a>
												</div>
												<!-- /.box-body -->
											</div>
										<!-- /.box -->
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<?php echo $this->Form->input('authorized_by', array('class' => 'form-control', 'label' => 'Autorizado por', 'div' => false, 'value' => $this->data[ 'User' ][ 'name' ], 'disabled' => 'disabled'));?>
											</div>
											<!-- /.form-group -->

											<div class="form-group">
												<?php echo $this->Form->input('authorized_date', array('class' => 'form-control', 'label' => 'Data da autorização', 'div' => false, 'disabled' => 'disabled', 'type' => 'text'));?>
											</div>
											<!-- /.form-group -->
										</div>
									</div>
								</div><!-- /.row -->
							</div><!-- /.box-body -->

							<?php
							}
							?>

							<div class="box-footer">
					            <?php
					            if ( empty( $onlyView  ) || $this->request->action == 'authorize' ) {
									echo $this->Html->link(
									  '<i class="fa fa-times"></i> Cancelar',
									  'index',
									  array('class' => 'btn btn-warning', 'escape' => false)
									);
									echo '&nbsp;';
									echo '&nbsp;';
									echo $this->Form->button(
									  '<i class="fa fa-check"></i> Salvar',
									  array('class' => 'btn btn-success ', 'escape' => false)
									);
					            } else {
									echo $this->Html->link(
									  '<i class="fa fa-undo"></i> Voltar',
									  'index',
									  array('class' => 'btn btn-warning', 'escape' => false)
									);
								}
					            ?>
					        </div>			
						</div><!-- /.box -->
					</div>
        		<?php echo $this->Form->end(); ?>
			</div>
		</div>

	</div>
</section>
<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('/manager/js/plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('/manager/js/bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->
<script src="/manager/js/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<?php echo $this->Html->script('/manager/js/dist/js/app.min'); ?>
