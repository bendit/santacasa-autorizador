<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Solicitações
	</h1>
	<ol class="breadcrumb">
		<li class='active'><i class="fa fa-line-chart"></i>&nbsp;Home</li>
		<li>Solicitações</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header box-header-dataTable">
					<h3 class="box-title"></h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<table class="table table-hover" id="data">
						<thead>
							<tr>
								<th>#</th>
								<th>Nome</th>
								<th>Número do Cartão</th>
								<th>Status</th>
								<th>Data da Solicitação</th>
								<th>Data da Autorização</th>
								<th>Autorizado por</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ( $requests as $r ) {
							?>
							<tr>
								<td><?php echo $r[ 'Request' ][ 'id' ];?></td>
								<td><?php echo $r[ 'Customer' ][ 'name' ];?></td>
								<td><?php echo $r[ 'Customer' ][ 'card_number' ];?></td>
								<td><?php echo $r[ 'Request' ][ 'status_description' ];?></td>
								<td><?php echo $r[ 'Request' ][ 'created' ];?></td>
								<td><?php echo $r[ 'Request' ][ 'authorized_date' ];?></td>
								<td><?php echo $r[ 'User' ][ 'name' ];?></td>
								<td class="text-center">
									<?php

									if ( AuthComponent::user( 'type' ) == 'administrator' && $r[ 'Request' ][ 'status' ] != 'closed' ) {
										echo $this->Html->link( '<i class="fa fa-edit"></i> Editar', array( 'action' => 'edit', $r[ 'Request' ][ 'id' ] ), array('class' => 'btn btn-warning btn-xs', 'escape' => false ) );
									}

									if ( $r[ 'Request' ][ 'status' ] != 'closed' ) {
										echo $this->Html->link( '<i class="fa fa-eye"></i> Visualizar', array( 'action' => 'view', $r[ 'Request' ][ 'id' ] ), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'style' => 'margin-left: 5px' ) );
										echo $this->Html->link( '<i class="fa fa-send"></i> Autorizar', array( 'action' => 'authorize', $r[ 'Request' ][ 'id' ] ), array('class' => 'btn btn-success btn-xs', 'escape' => false, 'style' => 'margin-left: 5px' ) );
									} else {
										echo $this->Form->postLink( '<i class="fa fa-send"></i> Enviar por E-mail', array('action' => 'email', $r[ 'Request' ][ 'id' ]), array('class' => 'btn btn-success btn-xs', 'escape' => false, 'style' => 'margin-left: 5px' ), __('Tem certeza que deseja enviar a autorização por e-mail?') ); 
										echo $this->Html->link( '<i class="fa fa-eye"></i> Visualizar', array( 'action' => 'view', $r[ 'Request' ][ 'id' ] ), array('class' => 'btn btn-primary btn-xs', 'escape' => false, 'style' => 'margin-left: 5px' ) );
									}

									if ( AuthComponent::user( 'type' ) == 'administrator' && $r[ 'Request' ][ 'status' ] != 'closed' ) {
										echo $this->Form->postLink( '<i class="fa fa-times"></i> Excluir', array('action' => 'delete', $r[ 'Request' ][ 'id' ]), array('class' => 'btn btn-danger btn-xs', 'escape' => false, 'style' => 'margin-left: 5px' ), __('Tem certeza que deseja excluir a solicitação # %s?', $r[ 'Request' ][ 'id' ]) ); 
									}
								?>
								</td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>
<!-- Datatables -->
<?php echo $this->Html->css(array('/manager/css/plugins/datatables/dataTables.bootstrap.css'));?>
<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('/manager/js/plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('/manager/js/bootstrap/js/bootstrap.min'); ?>
<?php echo $this->Html->script('/manager/js/plugins/datatables/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('/manager/js/plugins/datatables/dataTables.bootstrap.min.js'); ?>
<?php echo $this->Html->script('/manager/js/plugins/slimScroll/jquery.slimscroll.min.js');?>
<?php echo $this->Html->script('/manager/js/plugins/fastclick/fastclick.min.js');?>
<!-- AdminLTE App -->
<?php echo $this->Html->script('/manager/js/dist/js/app.min'); ?>


<script>
  	$( function() {
		$('#data').DataTable({
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_ resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": "Próximo",
			        "sPrevious": "Anterior",
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}   
		});
 	});
</script>
