<footer class="main-footer">
	<div class="container">
		<div class="col-md-12">
			<?php echo $this->element('sql_dump');?>
			<div class="pull-right hidden-xs">
				<strong>
					Copyright &copy; <?php echo date('Y'); ?>
				</strong>
			</div>
		</div>
	</div>
</footer>