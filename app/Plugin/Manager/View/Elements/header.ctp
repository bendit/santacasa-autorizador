	<header class="main-header">
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
        	<div class="container">

				<div class="navbar-header pull-left">
					<a href="/manager" class="navbar-brand" style="margin-left: 2px;">
						<?php echo $this->Html->image('/manager/img/santa_casa_logotipo.png', array('alt' => 'Manager', 'class' => 'logo-top'));?>
					</a>
		        </div>
						
				<div class="pull-left menu-topo col-md-offset-2">	
					<div class="collapse navbar-collapse " id="navbar-collapse">
			          <ul class="nav navbar-nav">
						<li class="<?php echo ( isset( $this->request->controller ) && $this->request->controller == 'dashboard' ) ? 'active' : ''; ?>">
							<?php
								echo $this->Html->link(
									'<i class="fa fa-dashboard"></i> <span>Dashboard</span> ',
									'/manager/dashboard',
									array('escape' => false)
								);
							?>
						</li>
						<li class="<?php echo ( isset( $this->request->controller ) && $this->request->controller == 'requests' ) ? 'active' : ''; ?> dropdown">
							<?php
								echo $this->Html->link(
									'<i class="fa fa-line-chart"></i> <span>Solicitações</span>',
									'/manager/requests',
									array('escape' => false)
								);
							?>
				        </li>
						<li class="<?php echo ( isset( $this->request->controller ) && $this->request->controller == 'reports' ) ? 'active' : ''; ?>">
							<?php
								echo $this->Html->link(
									'<i class="fa fa-clock-o"></i> <span>Relatórios</span>',
									'/manager/reports',
									array('escape' => false)
								);
							?>
						</li>
						<?php
						if ( AuthComponent::user( 'type' ) == 'administrator' ) {
						?>
						<li class="<?php echo ( isset( $this->request->controller ) && $this->request->controller == 'users' ) ? 'active' : ''; ?>">
							<?php
								echo $this->Html->link(
									'<i class="fa fa-users"></i> <span>Usuários</span>',
									'/manager/users',
									array('escape' => false)
								);
							?>
						</li>
						<?php
						}
						?>
						<li class="<?php echo ( isset( $this->request->controller ) && $this->request->controller == 'guides' ) ? 'active' : ''; ?>">
							<?php
								echo $this->Html->link(
									'<i class="fa fa-map"></i> <span>Guia Médico</span>',
									'/manager/guides',
									array('escape' => false)
								);
							?>
						</li>
			          </ul>
			        </div>
			     </div>

				<!-- Navbar Right Menu -->
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">

						<li class="visible-xs">
							<a class="navbar-toggler pull-right" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</a>
						</li>

						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<?php echo $this->Html->image( MANAGER_USER_PHOTOS_FILES_URL . AuthComponent::user('photo'), array('alt' => 'User Image', 'class' => 'user-image'));?>
								<span class="hidden-xs">
									<?php echo substr( AuthComponent::user('name'), 0, 15);?>
								</span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
									<?php echo $this->Html->image( MANAGER_USER_PHOTOS_FILES_URL . AuthComponent::user('photo'), array('alt' => 'User Image', 'class' => 'img-circle'));?>
									<p>
										<?php echo AuthComponent::user('name');?>
										<small>
											Membro desde 
											<?php 
												echo date("M", strtotime( AuthComponent::user('created') ));
												echo ". ";
												echo date("Y", strtotime(AuthComponent::user('created')));
											?>
										</small>
									</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<?php
											echo $this->Html->link(
												'Perfil',
												'/manager/users/profile',
												array('class' => 'btn btn-default btn-flat', 'escape' => false)
											);
										?>
									</div>
									<div class="pull-right">
									<?php
										echo $this->Html->link(
											'Sair',
											'/manager/login/logout',
											array('class' => 'btn btn-default btn-flat', 'escape' => false)
										);
									?>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</header>