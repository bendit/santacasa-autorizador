<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Manager</title>
        <link rel=icon href=/manager/img/favicon.png sizes="16x16" type="image/png">
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <?php echo $this->Html->css(array('/manager/css/bootstrap/css/bootstrap.min'));?>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- jvectormap -->
        <?php //echo $this->Html->css(array('plugins/jvectormap/jquery-jvectormap-1.2.2.css'));?>

        <?php echo $this->Html->css(array('/manager/css/plugins/select2/select2.min.css'));?>

        <!-- Theme style -->
        <?php echo $this->Html->css(array('/manager/css/dist/css/AdminLTE'));?>

        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <?php echo $this->Html->css(array('/manager/css/dist/css/skins/_all-skins.min'));?>

        <!-- Custom CSS -->
        <?php echo $this->Html->css(array('/manager/css/custom'));?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
        .dropdown-submenu {
            position: relative;
        }

        .dropdown-submenu .dropdown-menu {
            top: 0;
            left: 100%;
            margin-top: -1px;
        }
        </style>
    </head>
    <body class="sidebar-mini skin-black layout-top-nav">
        <div class="wrapper">
            <?php echo $this->element('header');?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="container">
                    <section class="content-header header-margin-top">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $this->Session->flash();?>
                            </div>
                        </div>
                    </section>
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div><!-- /.content-wrapper -->
            <?php echo $this->element('footer');?>
        </div><!-- ./wrapper -->
        <script type="text/javascript">
            $( function() {
                $( '#flashMessage' ).slideDown( 'slow' );
                setTimeout( function() {
                    $( '#flashMessage' ).slideUp( 'slow' );
                }, 5000);
            });
        </script>
    </body>
</html>
