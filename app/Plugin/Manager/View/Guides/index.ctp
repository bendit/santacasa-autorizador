<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Guia Médico
		<?php echo $this->Html->link( '<i class="fa fa-pencil"></i> Novo', 'add', array('class' => 'btn btn-primary btn-xs', 'escape' => false ) );?>
	</h1>
	<ol class="breadcrumb">
		<li class='active'><i class="fa fa-map"></i>&nbsp;Home</li>
		<li>Guia Médico</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header box-header-dataTable">
					<h3 class="box-title"></h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<table class="table table-hover" id="data">
						<thead>
							<tr>
								<th>Título</th>
								<th>Data de Envio</th>
								<th>Por Usuário</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ( $guides as $g ) {
							?>
							<tr>
								<td><?php echo $this->Html->link( $g[ 'Guide' ][ 'title' ], MANAGER_GUIDES_URL . $g[ 'Guide' ][ 'filename' ], array( 'target' => '_blank' ) );?></td>
								<td><?php echo $g[ 'Guide' ][ 'created' ];?></td>
								<td><?php echo $g[ 'User' ][ 'name' ];?></td>
								<td>
									<?php echo $this->Html->link( '<i class="fa fa-pencil"></i> Editar', array( 'action' => 'edit', $g[ 'Guide' ][ 'id' ] ), array('class' => 'btn btn-warning btn-xs', 'escape' => false ) );?>
									<?php 
									echo $this->Form->postLink( '<i class="fa fa-times"></i> Excluir', array('action' => 'delete', $g[ 'Guide' ][ 'id' ], $g[ 'Guide' ][ 'filename' ]), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Tem certeza que deseja excluir o usuário # %s?', $g[ 'Guide' ][ 'title' ]) ); 
								?>
								</td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>
<!-- Datatables -->
<?php echo $this->Html->css(array('/manager/css/plugins/datatables/dataTables.bootstrap.css'));?>
<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('/manager/js/plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('/manager/js/bootstrap/js/bootstrap.min'); ?>
<?php echo $this->Html->script('/manager/js/plugins/datatables/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('/manager/js/plugins/datatables/dataTables.bootstrap.min.js'); ?>
<?php echo $this->Html->script('/manager/js/plugins/slimScroll/jquery.slimscroll.min.js');?>
<?php echo $this->Html->script('/manager/js/plugins/fastclick/fastclick.min.js');?>
<!-- AdminLTE App -->
<?php echo $this->Html->script('/manager/js/dist/js/app.min'); ?>


<script>
  	$( function() {
		$('#data').DataTable({
			"language": {
				"sEmptyTable": "Nenhum registro encontrado",
			    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
			    "sInfoPostFix": "",
			    "sInfoThousands": ".",
			    "sLengthMenu": "_MENU_ resultados por página",
			    "sLoadingRecords": "Carregando...",
			    "sProcessing": "Processando...",
			    "sZeroRecords": "Nenhum registro encontrado",
			    "sSearch": "Pesquisar",
			    "oPaginate": {
			        "sNext": "Próximo",
			        "sPrevious": "Anterior",
			        "sFirst": "Primeiro",
			        "sLast": "Último"
			    },
			    "oAria": {
			        "sSortAscending": ": Ordenar colunas de forma ascendente",
			        "sSortDescending": ": Ordenar colunas de forma descendente"
			    }
			}   
		});
 	});
</script>
