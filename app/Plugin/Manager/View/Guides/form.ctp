<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Guia Médico
	</h1>
	<ol class="breadcrumb">
		<li class='active'><i class="fa fa-map"></i>&nbsp;Home</li>
		<li>Guia Médico</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
        		<?php echo $this->Form->create( 'Guide', array( 'type' => 'file' ) ); ?>
        			<?php echo $this->Form->input('id');?>
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Adicionar Guia Médico</h3>
								<div class="box-tools pull-right">
								</div>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<?php echo $this->Form->input('title', array('class' => 'form-control', 'label' => 'Título', 'div' => false));?>
										</div>
										<!-- /.form-group -->
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label>Arquivo</label>
											<?php echo $this->Form->file('file', array('class' => 'form-control', 'label' => 'Nome', 'div' => false));?>
										</div>
										<!-- /.form-group -->
									</div>
								</div><!-- /.row -->
							</div><!-- /.box-body -->
							<div class="box-footer">
					            <?php
									echo $this->Html->link(
									  '<i class="fa fa-times"></i> Cancelar',
									  'index',
									  array('class' => 'btn btn-warning', 'escape' => false)
									);
									echo '&nbsp;';
									echo '&nbsp;';
									echo $this->Form->button(
									  '<i class="fa fa-check"></i> Salvar',
									  array('class' => 'btn btn-success ', 'escape' => false)
									);
					            ?>
					        </div>			
						</div><!-- /.box -->
					</div>
        		<?php echo $this->Form->end(); ?>
			</div>
		</div>

	</div>
</section>
<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('/manager/js/plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('/manager/js/bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->
<script src="/manager/js/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
<?php echo $this->Html->script('/manager/js/dist/js/app.min'); ?>