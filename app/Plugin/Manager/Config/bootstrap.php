<?php
if ( !defined( 'FILES_URL' ) )
	define('FILES_URL', '/files/');

if ( !defined( 'FILES_REQUESTS_URL' ) )
	define('FILES_REQUESTS_URL', '/files/requests/');

if ( !defined( 'MANAGER' ) )
	define( 'MANAGER', APP . 'Plugin' . DS . 'Manager' . DS );

if ( !defined( 'MANAGER_FILES' ) )
	define( 'MANAGER_FILES', APP . 'Plugin' . DS . 'Manager' . DS . 'webroot' . DS . 'files' . DS );

if ( !defined( 'MANAGER_FILES_URL' ) )
	define( 'MANAGER_FILES_URL', '/manager/files/' );

if ( !defined( 'MANAGER_USER_PHOTOS_FILES' ) )
	define( 'MANAGER_USER_PHOTOS_FILES', APP . 'Plugin' . DS . 'Manager' . DS . 'webroot' . DS . 'files' . DS . 'user_photos' . DS );

if ( !defined( 'MANAGER_USER_PHOTOS_FILES_URL' ) )
	define( 'MANAGER_USER_PHOTOS_FILES_URL', '/manager/files/user_photos/' );

if ( !defined( 'MANAGER_GUIDES_DIR' ) )
	define( 'MANAGER_GUIDES_DIR', APP . 'Plugin' . DS . 'Manager' . DS . 'webroot' . DS . 'files' . DS . 'guides' . DS );

if ( !defined( 'MANAGER_GUIDES_URL' ) )
	define( 'MANAGER_GUIDES_URL', '/manager/files/guides/' );

if ( !defined( 'MANAGER_REQUESTS_DIR' ) )
	define( 'MANAGER_REQUESTS_DIR', APP . 'Plugin' . DS . 'Manager' . DS . 'webroot' . DS . 'files' . DS . 'requests' . DS );

if ( !defined( 'MANAGER_REQUESTS_URL' ) )
	define( 'MANAGER_REQUESTS_URL', '/manager/files/requests/' );