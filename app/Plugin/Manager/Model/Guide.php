<?php
App::uses('AppModel', 'Model');

class Guide extends AppModel {

	public function beforeSave($options = array()) {
		
		$this->data['Guide']['user_id'] = AuthComponent::user( 'id' );
	    
	    return true;
	}

    public function afterFind( $results, $primary = false ) {

        foreach ($results as $key => $v) {
            $results[ $key ]['Guide']['created'] = date( 'd/m/Y H:i:s', strtotime( $v[ 'Guide' ][ 'created' ] ) );
        }

        unset ( $this->validate[ 'file' ] ); // Remove validação do password quando for editar

        return $results;
    }

	public function beforeValidate($options = array()){
	    if ( !empty( $this->data[ 'Guide' ][ 'id' ] ) ) {
	         $this->validator()->remove( 'file' );
	    }
	}

	public $belongsTo = array( 'User' );

/**
 * Validação
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo título é obrigatório'
			),
		),
		'file' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo arquivo é obrigatório'
			)
		)
	);
}
