<?php
App::uses('AppModel', 'Model');

class Request extends AppModel {

	public $status = array( 'new' => 'Novo', 'attendance' => 'Em atendimento', 'closed' => 'Autorizado', 'deleted' => 'Excluído' );

	public function beforeSave($options = array()) {
		if ( $this->data['Request']['appointment'] == '-' )
			unset( $this->data['Request']['appointment'] );

	    return true;
	}

	public function afterSave( $created, $options = array() ) {
		if ( $created ) {
			$this->data[ 'Request' ][ 'card_photo_front' ] 	  = $this->base64ToImage( $this->data[ 'Request' ][ 'card_photo_front_full' ], $this->id );
			$this->data[ 'Request' ][ 'card_photo_back' ] 	  = $this->base64ToImage( $this->data[ 'Request' ][ 'card_photo_back_full' ], $this->id );
			$this->data[ 'Request' ][ 'guide' ] 	  		  = $this->base64ToImage( $this->data[ 'Request' ][ 'guide_full' ], $this->id );
			
			$this->data[ 'Request' ][ 'hash' ] = md5( $this->id . $this->data['Request'][ 'customer_id' ] );

			$this->save( $this->data );
		}
		
		return true;
	}

    public function afterFind( $results, $primary = false ) {

        foreach ($results as $key => $v) {
            $results[ $key ]['Request']['created'] = date( 'd/m/Y H:i:s', strtotime( $v[ 'Request' ][ 'created' ] ) );

            if ( isset( $v[ 'Request' ][ 'authorized_date' ] ) && $v[ 'Request' ][ 'authorized_date' ] != '0000-00-00' )
            	$results[ $key ]['Request']['authorized_date'] = date( 'd/m/Y H:i:s', strtotime( $v[ 'Request' ][ 'authorized_date' ] ) );
            else
            	$results[ $key ]['Request']['authorized_date'] = '-';

            if ( isset( $v[ 'Request' ][ 'appointment' ] ) && $v[ 'Request' ][ 'appointment' ] != '0000-00-00' )
            	$results[ $key ]['Request']['appointment'] = date( 'd/m/Y H:i:s', strtotime( $v[ 'Request' ][ 'appointment' ] ) );
            else
            	$results[ $key ]['Request']['appointment'] = '-';

            $results[ $key ]['Request']['status_description'] = $this->status[ $v[ 'Request' ][ 'status' ] ];
        }

        return $results;
    }

	public function base64ToImage( $img = null, $id = null ) {

		$img = explode( ',', $img );

    	if ( !file_exists( MANAGER_REQUESTS_DIR ) ) 
    		mkdir( MANAGER_REQUESTS_DIR );

    	if ( !file_exists( MANAGER_REQUESTS_DIR . $id . '/' ) ) 
    		mkdir(MANAGER_REQUESTS_DIR . $id . '/' );

		$filename = md5( time() . uniqid() ).".jpg"; 
		$decoded  = base64_decode( $img[ 1 ] ); 
		file_put_contents( MANAGER_REQUESTS_DIR . $id . '/' . $filename, $decoded );

		return $filename;
	}

	public $belongsTo = array( 'Customer', 'User' );

/**
 * Validação
 *
 * @var array
 */
	public $validate = array(
		'local' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo local do procedimento é obrigatório'
			),
		),
		'card_photo_front' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo foto da frente do cartão é obrigatório'
			),
		),
		'card_photo_back' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo foto do verso do cartão é obrigatório'
			),
		),
		'guide' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo foto da guia do médico é obrigatório'
			)
		)
	);
}
