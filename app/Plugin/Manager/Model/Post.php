<?php
App::uses('AppModel', 'Model');

class Post extends AppModel {

	public function beforeSave($options = array()) {
		$this->data['Post']['post']   = file_get_contents('php://input');
		$this->data['Post']['status'] = 'new';	    
	    
	    return true;
	}
}
