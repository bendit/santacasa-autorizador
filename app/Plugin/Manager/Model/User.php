<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

	public $types = array( 'administrator' => 'Administrador', 'analyst' => 'Analista' );

	public function beforeSave($options = array()) {
		
		if( empty( $this->data['User']['password'] ) )
			unset( $this->data['User']['password'] );
		else
			$this->data['User']['password'] = AuthComponent::password( $this->data['User']['password'] );
	    
	    return true;
	}

    public function afterFind( $results, $primary = false ) {

        foreach ($results as $key => $v) {
            
            if ( isset( $v[ 'User' ][ 'status' ] ) )
            	$results[ $key ]['User']['status_description'] = ( $v[ 'User' ][ 'status' ] == 'Active' ) ? 'Ativo': 'Suspenso';
            
            if ( isset( $v[ 'User' ][ 'type' ] ) )
            	$results[ $key ]['User']['type_description'] = $this->types[ $v[ 'User' ][ 'type' ] ];

            if ( isset( $v[ 'User' ][ 'password' ] ) ) {
                $results[ $key ]['User']['password_'] = $v[ 'User' ][ 'password' ];
                $results[ $key ]['User']['password'] = '';
            }
        }

        unset ( $this->validate[ 'password' ] ); // Remove validação do password quando for editar

        return $results;
    }

	public function beforeValidate($options = array()){
	    if ( !empty( $this->data[ 'User' ][ 'id' ] ) ) {
	         $this->validator()->remove( 'password' );
	    }
	}

/**
 * Validação
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo nome é obrigatório'
			),
		),
		'password' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo senha é obrigatório'
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Campo e-mail é obrigatório'
			),
			'isUnique' => array(
				'rule' => array('isUnique'),
				'message' => 'O endereço de e-mail informado j&aacute; est&aacute sendo utilizado'
			)
		),
		'senha' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo senha é obrigatório'
			),
		),
		'status' => array(
			'notempty' => array(
				'rule' => array('notBlank'),
				'message' => 'Campo status é obrigatório'
			)
		)
	);
}
