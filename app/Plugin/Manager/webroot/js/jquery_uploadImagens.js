$(function () {
    var url = '/dashboard/UploadImagem/php/'; // Upload Images
    
    $('#fileuploadBackgroundImage').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
            	if(file.error)
            		alert(file.error);
            	else{
                    var id = Math.floor((Math.random() * 10000000) + 1);
                    html = "<div class='col-md-3 "+id+"'><input class='creatives' name='images[]' type='hidden' value='"+file.name+"'><img src='../../files/"+file.name+"' width='100%'/>";
                    html += "<a href='javascript:;' onClick='fncRemoverImagemUpload("+id+")' class='remover-img-upload text-red pull-right'>Remover</a><br /></div>";
                    
                    $( ".images" ).append( html );
                    
                    $('#progress .progress-bar').css(
                        'width',
                        '0%'
                    );

                    $( "#CreativeGroupHeadlines" ).blur();
	            }
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);         
			$('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

function fncRemoverImagemUpload(id){
	$( "."+id ).remove();
    $( "#CreativeGroupHeadlines" ).blur();
}