
	function selectPostIn(post_in){
		
		$.ajax({
			type: "GET",
			url: '/posts/getListPagesOrGroup',
			data: {post_in: post_in},
			async: false,
			success: function(data) {
				if(data != 'alerta_token'){
					if(post_in == 'page'){
			  			$(".combo_list_pages").show();
			  			$(".combo_list_groups").hide();
			  			$(".combo_list_groups").html("");
			  			$(".combo_list_pages").html(data);
			  		}
			  		if(post_in == 'groups'){
						$(".combo_list_groups").show();
						$(".combo_list_pages").hide();
						$(".combo_list_pages").html("");
			  			$(".combo_list_groups").html(data);
					}
					if(post_in == 'profile'){
						$(".combo_list_pages").hide();
						$(".combo_list_groups").hide();
					}
				}else{
					$(".data-publicacao .radio").hide();
					$(".alerta_token").show();
				}
			},
			error: function(data){
				console.log(data);
			}
		});
	}

	function prevQuestions(count){
		var $curr = $( "#boxQuestionPreview"+count);
		$curr = $curr.prev();
		$( ".telas" ).css( "display", "none" );
		$curr.css( "display", "block" );
	}
	
	function nextQuestions(count){
		var $curr = $( "#boxQuestionPreview"+count);
		$curr = $curr.next();
		$( ".telas" ).css( "display", "none" );
		$curr.css( "display", "block" );
	}

	function date_countdown(date){
		
		$('#clock').countdown(date).on('update.countdown', function(event) {
			var format = ''
		     + '<span class="countdown_hours"> <span class="num">%H</span> <span class="desc_countdown">Horas</span></span> '
		     + '<span class="divisao_countdown">:</span>'
		     + '<span class="countdown_min"> <span class="num">%M</span> <span class="desc_countdown">Min</span></span> '
		     + '<span class="divisao_countdown">:</span>'
		     + '<span class="countdown_sec"> <span class="num">%S</span> <span class="desc_countdown">Sec</span></span>';

			if(event.offset.days > 0) {
				format = '<span class="countdown_days"> <span class="num">%d</span> <span class="desc_countdown">Dias</span></span> ' + '<span class="divisao_countdown">:</span>' + format;
			}
			if(event.offset.weeks > 0) {
				format = '<span class="countdown_weeks"> <span class="num">%w</span> <span class="desc_countdown">Sem</span></span> ' + '<span class="divisao_countdown">:</span>' + format;
			}
			
			$(this).html(event.strftime(format));
			}).on('finish.countdown', function(event) {
				$(this).html('This offer has expired!')
				.parent().addClass('disabled');

			});		  		
	}

	function fnc_add_pergunta(){

		var count = $("#count_questions").val();
		count = parseInt(count) + 1;
		$("#count_questions").val(count);
		
		$.ajax({
			type: "GET",
			url: '/posts/campoPergunta',
			data: {count: count},
			async: false,
			success: function(data) {
		  		$("#accordion").append(data);

		  		fncAddPreview(count);
			}
		});
	}

	function fncVisializarPreview(count){
		$(".telas").hide();
		$("#boxQuestionPreview"+count).show();
	}

	function fncRemoveQuestion(id){
		var view = $("#view").val();

		if(view == 'add'){
			$("#boxQuestion"+id).remove();
			$("#boxQuestionPreview"+id).remove();
		}
		if(view == 'edit'){
			$.ajax({
				type: "GET",
				url: '/posts/fncRemoveQuestion',
				data: {id: id},
				async: false,
				success: function(data) {
					if(data == 'excluido'){
						$("#boxQuestion"+id).remove();
						$("#boxQuestionPreview"+id).remove();
					}
					if(data == 'nao_excluido'){
						$("#boxQuestion"+id).remove();
						$("#boxQuestionPreview"+id).remove();
					}
				}
			});			
		}

	}

	function fncAddPreview(count){
		$.ajax({
			type: "GET",
			url: '/posts/previewQuestion',
			data: {count: count},
			async: false,
			success: function(data) {
				$(".polls div.tela-envio-resposta").before(data);

				var strong = $("#PostCustomizationHeadlineColor").val();
				var description = $("#PostCustomizationDescriptionColor").val();
				var btn = $("#PostCustomizationButtonColor").val();
				var btnDescription = $("#PostCustomizationButtonDescriptionColor").val();

				$(".questions-replies strong").attr('style', 'color:'+strong);
				$(".questions-replies li").attr('style', 'color:'+description);
				$(".btn-prev-questions, .btn-next-questions").attr('style', 'background-color:'+btn);
				$(".btn-prev-questions span, .btn-next-questions span").attr('style', 'color:'+btnDescription);
			}
		});
	}

	function previewQuestionReplieText(count, campo){
		var question = $("textarea#QuestionQuestion"+count).val();
		var option_a = $("#ReplieOption_a"+count).val();
		var option_b = $("#ReplieOption_b"+count).val();
		var option_c = $("#ReplieOption_c"+count).val();
		var option_d = $("#ReplieOption_d"+count).val();

		$("#boxQuestionPreview"+count+" #previewQuestion").html(question);
		$("#boxQuestionPreview"+count+" #previewReplieOption_a").html(option_a);
		$("#boxQuestionPreview"+count+" #previewReplieOption_b").html(option_b);
		$("#boxQuestionPreview"+count+" #previewReplieOption_c").html(option_c);
		$("#boxQuestionPreview"+count+" #previewReplieOption_d").html(option_d);
	}