	$(function () {
		var headline_limit = 40;
		var description_limit = 120;
		var btn_enviar_limit = 15;

		// Ads
		var ads_headline_limit = 70;
		var ads_description_limit = 150;
		var ads_description_link_limit = 15;

		if($("#PostIsScheduled").val() == "NO"){
			$(".data-publicacao").hide();
		}
		if($("#PostBasicOptionIsCountdown").val() == "NO"){
			$(".data-countdown").hide();
		}
		if($("#PostBasicOptionIsProductImage").val() == "NO"){
			$(".product-image").hide();
		}

		$(".alerta_token").hide();

		// ========================================================================================
		// Count Caracter restante para o edit
		// Campos: AdsHeadLine, AdsDescription, AdsDescriptionLink
		// PostBasicOptionHeadline, PostBasicOptionDescription e PostCustomizationButtonDescription
		// ========================================================================================
			if($("#PostConfigurationType").val() != "Optin" && $("#PostConfigurationType").val() != "None"){
				var numCaracterAdsHeadlineEdit = $("#AdsHeadLine").val().length;
				numCaracterAdsHeadlineEdit = ads_headline_limit - numCaracterAdsHeadlineEdit;
				$("#caracterHead").html(numCaracterAdsHeadlineEdit);

				var numCaracterAdsDescription = $("#AdsDescription").val().length;
				numCaracterAdsDescription = ads_description_limit - numCaracterAdsDescription;
				$("#caracterDes").html(numCaracterAdsDescription);
				
				if($("#PostConfigurationType").val() != "Ratings" && $("#PostConfigurationType").val() != "Share"){
					var numCaracterAdsBtnEnviar = $("#AdsDescriptionLink").val().length;
					numCaracterAdsBtnEnviar = ads_description_link_limit - numCaracterAdsBtnEnviar;
					$("#caracterDesLink").html(numCaracterAdsBtnEnviar);
				}
			}

			if($("#PostConfigurationType").val() == "Optin"){
				var numCaracterHeadlineEdit = $("#PostBasicOptionHeadline").val().length;
				numCaracterHeadlineEdit = headline_limit - numCaracterHeadlineEdit;
				$("#caracterHead").html(numCaracterHeadlineEdit);
				
				var numCaracterDescription = $("#PostBasicOptionDescription").val().length;
				numCaracterDescription = description_limit - numCaracterDescription;
				$("#caracterDes").html(numCaracterDescription);
				
				var numCaracterBtnEnviar = $("#PostCustomizationButtonDescription").val().length;
				numCaracterBtnEnviar = btn_enviar_limit - numCaracterBtnEnviar;
				$("#caracterBotao").html(numCaracterBtnEnviar);
			}

		// ========================================================================================
		// Limite de caracter para os campos:
		// AdsHeadLine, AdsDescription, AdsDescriptionLink
		// PostBasicOptionHeadline, PostBasicOptionDescription e PostCustomizationButtonDescription
		// ========================================================================================

			// ===================
			// keyup para Post Ads
			// ===================
				$("#AdsHeadLine").keyup( function(){

					var text = $("#AdsHeadLine").val();

					var caracter = ads_headline_limit - text.length;
					$("#caracterHead").html(caracter);

					if (text.length <= ads_headline_limit){
						$(".headline").html(text);
					}

				});
				$("#AdsDescription").keyup( function(){

					var text = $("#AdsDescription").val();
					
					var caracter = ads_description_limit - text.length;
					$("#caracterDes").html(caracter);

					if (text.length <= ads_description_limit)
						$(".descricao").html(text);
				});
				$("#AdsDescriptionLink").keyup( function(){

					var text = $("#AdsDescriptionLink").val();
					
					var caracter = ads_description_link_limit - text.length;
					$("#caracterDesLink").html(caracter);

					if (text.length <= ads_description_link_limit)
						$(".link-cta span").html(text);
				});

			// ===================================
			// keyup para Todos os Post exceto Ads
			// ===================================
				$("#PostBasicOptionHeadline").keyup( function(){

					var text = $("#PostBasicOptionHeadline").val();

					var caracter = headline_limit - text.length;
					$("#caracterHead").html(caracter);

					if (text.length <= headline_limit){
						$(".headline").html(text);
					}
				});

				$("#PostBasicOptionDescription").keyup( function(){

					var text = $("#PostBasicOptionDescription").val();
					
					var caracter = description_limit - text.length;
					$("#caracterDes").html(caracter);

					if (text.length <= description_limit)
						$(".descricao").html(text);
				});

				$("#PostCustomizationButtonDescription").keyup( function(){
					
					var text = $("#PostCustomizationButtonDescription").val();
					
					var caracter = btn_enviar_limit - text.length;
					$("#caracterBotao").html(caracter);

					if (text.length <= btn_enviar_limit)
						$(".btn_enviar span").html( text );
				});

				$("#PostConfigurationCompanyName").keyup( function(){
					
					var text = $("#PostConfigurationCompanyName").val();
					
					$(".box-logo-empresa span").html( text );
					
				});
		
			// ==================
			// Cores customizacao 
			// ==================
				$("#PostCustomizationHeadlineColor").colorpicker().on('changeColor.colorpicker', function(event){
					var cor = $("#PostCustomizationHeadlineColor").val();
					$(".headline").attr('style', 'color:' + cor);
					$(".questions-replies strong").attr('style', 'color:' + cor);
				});
				
				$("#PostCustomizationDescriptionColor").colorpicker().on('changeColor.colorpicker', function(event){
					var cor = $("#PostCustomizationDescriptionColor").val();
					$(".descricao").attr('style', 'color:' + cor);
					$(".questions-replies li").attr('style', 'color:' + cor);
				});
				
				$("#PostCustomizationButtonDescriptionColor").colorpicker().on('changeColor.colorpicker', function(event){
					var cor = $("#PostCustomizationButtonDescriptionColor").val();
					$(".btn_enviar span").attr('style', 'color:' + cor);

					// Botão cta
					$(".link-cta span, .link-cta-enviar-respostas span").attr('style', 'color:' + cor);
					$(".btn-prev-questions span").attr('style', 'color:' + cor);
					$(".btn-next-questions span").attr('style', 'color:' + cor);
				});
				
				$("#PostCustomizationCompanyNameColor").colorpicker().on('changeColor.colorpicker', function(event){
					var cor = $("#PostCustomizationCompanyNameColor").val();
					$(".box-logo-empresa span").attr('style', 'color:' + cor);
				});
				
				$("#PostCustomizationButtonColor").colorpicker().on('changeColor.colorpicker', function(event){
					var cor = $("#PostCustomizationButtonColor").val();
					$(".btn_enviar").attr('style', 'background-color:' + cor);

					// Botão cta
					$(".link-cta, .link-cta-enviar-respostas").attr('style', 'background-color:' + cor); 
					$(".btn-prev-questions").attr('style', 'background-color:' + cor);  
					$(".btn-next-questions").attr('style', 'background-color:' + cor); 
				});

				$("#PostBasicOptionLink").blur( function(){
					var url = $("#PostBasicOptionLink").val();
					$(".link-cta").attr('href', url);
				});
			
			// =====================================================
			// exibir ou não campos com base nas opções selecionadas
			// =====================================================
				$("#PostAdvancedOptionButtonClose").change( function(){
					var select = $(this).val();
					
					if(select == 'YES'){
						$(".btn-close").show();
					}else{
						$(".btn-close").hide();
					}
				});
				
				$("#PostIsScheduled").change( function(){
					var select = $(this).val();
					
					if(select == 'YES'){
						$(".data-publicacao").show();
					}else{
						$(".data-publicacao").hide();
					}
				});
				
				$("#PostBasicOptionFieldName").change( function(){
					var select = $(this).val();
					
					if(select == 'YES'){
						$(".box-campo-nome").show();
						$(".box-campo-sobrenome").show();
						$(".option").removeClass('margin-headline');
					}else{
						$(".box-campo-nome").hide();
						$(".box-campo-sobrenome").hide();
						$(".option").addClass('margin-headline');
					}
				});
			
				$("#PostBasicOptionIsCountdown").change( function(){
					var select = $(this).val();
					
					if(select == 'YES'){
						$(".data-countdown").show();
						$(".countdown").show();
						$(".not-countdown").hide();
					}else{
						$(".data-countdown").hide();
						$(".countdown").hide();
						$(".not-countdown").show();
					}
				});
				
				$("#ShareFacebook").change( function(){
					var select = $(this).val();
					
					if(select == 'YES'){
						$("#icone-facebook").show();
					}else{
						$("#icone-facebook").hide();
					}
				});
			
				$("#ShareTwitter").change( function(){
					var select = $(this).val();
					
					if(select == 'YES'){
						$("#icone-twitter").show();
					}else{
						$("#icone-twitter").hide();
					}
				});
			
				$("#ShareGooglePlus").change( function(){
					var select = $(this).val();
					
					if(select == 'YES'){
						$("#icone-google").show();
					}else{
						$("#icone-google").hide();
					}
				});

				$("#PostConfigurationVideoUrl").blur( function(){
					var url = $(this).val();
					$(".video").attr('url', url);
				});

				$( ".link-polls" ).click(function() {
					var $curr = $( "#start" );
					$curr.css( "display", "block" );
					$curr = $curr.next();
					$( ".telas" ).css( "display", "none" );
					$curr.css( "display", "block" );
				});

				$("#PostBasicOptionIsProductImage").change( function(){
					var select = $(this).val();
					
					if(select == 'YES'){
						$(".product-image").show();
						$('.bg-cta-preview-jquery').removeClass('ads');
						$('.bg-cta-preview-jquery').addClass('product');
						$('.img-produto').show();
					}else{
						$(".product-image").hide();
						$('.bg-cta-preview-jquery').removeClass('product');
						$('.bg-cta-preview-jquery').addClass('ads');
						$('.img-produto').hide();
					}
				});
	});