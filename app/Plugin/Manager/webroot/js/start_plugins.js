	$(function () {

	    // ==========================================================
	    // Start daterangepicker campos: CountdownTime e CountdownCta
	    // ==========================================================
			$('#PostSchedule').daterangepicker({
				"singleDatePicker": true,
				"timePicker": true,
				"timePicker24Hour": true,
				"autoUpdateInput": false,
				"startDate": "01/01/2016",
				"endDate": "01/07/2016",
				"opens": "right",
				"drops": "up",
				format: 'MM/DD/YYYY H:m'
			}, function(start, end, label) {
				$("#PostSchedule").val(start.format('DD/MM/YYYY H:mm'));

			});
			
			$('#PostBasicOptionCountdownTime').daterangepicker({
				"singleDatePicker": true,
				"timePicker": true,
				"timePicker24Hour": true,
				"autoUpdateInput": false,
				"startDate": "01/01/2016",
				"endDate": "01/07/2016",
				"opens": "right",
				"drops": "up",
				format: 'MM/DD/YYYY H:m'
			}, function(start, end, label) {
				$("#PostBasicOptionCountdownTime").val(start.format('DD/MM/YYYY H:mm'));
				date_countdown(start.format('YYYY/MM/DD/ H:mm'));
			});
			
	});
