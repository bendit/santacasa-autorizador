<?php

App::uses('AppController', 'Controller');

class HomeController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public function index() {
		$this->layout = false;
		$this->set( 'title_for_layout', 'Santa Casa Saúde' );

		$this->set( 'url_site', 'http://www.santacasasaudesjc.com.br/' );
	}
}
