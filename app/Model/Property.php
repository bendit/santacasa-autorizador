<?php
App::uses('AppModel', 'Model');

class Property extends AppModel {

	public function afterFind( $results = array(), $primary = false ) {	
		foreach ( $results as $k => $r ) {
			$results[ $k ][ 'Property' ][ 'created' ] = date( 'd/m/Y', strtotime( $r[ 'Property' ][ 'created' ] ) );

			$diff = $this->dateDifference( $r[ 'Property' ][ 'created' ], date( 'Y-m-d' ) );

			if ( $diff < 1 )
				$results[ $k ][ 'Property' ][ 'published' ] = 'Publicado há menos de 24 horas';
			elseif ( $diff == 1 )
				$results[ $k ][ 'Property' ][ 'published' ] = 'Publicado há 1 dia atrás';
			else
				$results[ $k ][ 'Property' ][ 'published' ] = 'Publicado há ' . $diff . ' dias atrás';
		}

		return $results;
	}

	public function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
	{
	    $datetime1 = date_create($date_1);
	    $datetime2 = date_create($date_2);
	    
	    $interval = date_diff($datetime1, $datetime2);
	    
	    return $interval->format($differenceFormat);
	    
	}

	public $belongsTo = array( 'Location', 'Type' );

	public $hasMany = array( 
		'Photo' => array(
			'className' => 'Photo',
			'foreignKey' => 'property_id',
			'dependent' => true
		)
	);
}